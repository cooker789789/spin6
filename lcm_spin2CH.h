//穿梭II lcm_spin2CH.h  中文選物版本

unsigned char code wb5_coin[4][32]=
//投		
{0x00,0x00,0x00,0x00,0x21,0xE0,0x21,0x20,0x21,0x24,0xFA,0x24,0x22,0x24,0x2A,0x1C,
0x34,0x00,0x27,0xF0,0xE2,0x10,0x21,0x20,0x21,0x40,0x20,0xC0,0xA3,0x30,0x4C,0x0C,
//幣
0x00,0x00,0x00,0x00,0x10,0x40,0x56,0x40,0x91,0x7C,0xFE,0xC8,0x93,0x28,0xB6,0x10,
0xD2,0x28,0x96,0xC4,0x02,0x00,0x3F,0xF0,0x22,0x10,0x22,0x10,0x22,0x70,0x02,0x00,
//時		
0x00,0x00,0x00,0x00,0x00,0x40,0xF0,0x40,0x93,0xF8,0x90,0x40,0x90,0x40,0x97,0xFC,
0xF0,0x10,0x90,0x10,0x97,0xFC,0x92,0x10,0xF1,0x10,0x91,0x10,0x00,0x50,0x00,0x20,
//間				
0x00,0x00,0x00,0x00,0xFC,0xF8,0x84,0x88,0xFC,0xF8,0x84,0x88,0xFC,0xF8,0x80,0x08,
0x80,0x08,0x8F,0xC8,0x88,0x48,0x8F,0xC8,0x88,0x48,0x8F,0xC8,0x80,0x28,0x80,0x10};
//歡迎光臨!請投幣	
unsigned char code *Wwelcome={"歡迎光臨! 請投幣"};
unsigned char code    *WFSET={"  系統設定:     "};
//~感謝您~
unsigned char code  *w_thank_you={" ~  感謝您  ~   "};	

//禮品售價   元     
unsigned char code *w_gift_price={"禮品售價      元"};

//累加金額    元
unsigned char code  *w_money_add={"累加金額      元"};
//已到達選物金額
unsigned char code *wgift_money_ok={"已到達選物金額  "};
//一投  元每局  元
unsigned char code        *w_1play={"一投  元每局  元"};

//!!恭喜中獎!!
unsigned char code *Wconyouwin={"  !!恭喜中獎!!  "};
//!!祝您中獎!!
unsigned char code *Wpsyouwin={"  !!祝您中獎!!  "};

//歡迎光臨!請投幣/	~Insert Coins~
unsigned char code   *WInsert={"歡迎光臨!請投幣!"};	
unsigned char code  *Wcredits={"credits=     /  "};
//Play Till Win
unsigned char code *WplayTWin={" Play Till Win  "};	
//口口遊戲開始口口/	~Start Game~
unsigned char code    *Wstart={" ~  遊戲開始  ~ "};	
//  ~加油~/ ~Good Luck!!
unsigned char code     *Wgogo={"  ~ 加油  加油 ~"};
//!!祝您中獎!! / Have a good time
//unsigned char code *Wpsyouwin={"~  Good Luck  ~ "};
//~遊戲結束~ / ~Game Over~
unsigned char code *Wgameover={" ~  遊戲結束  ~ "};
//!!恭喜中獎!!/ Congratulation
unsigned char code  *Wcongrat={"<<< 恭喜中獎 >>>"};
//歡迎您再次光臨~ /Please Try again  
unsigned char code  *Wnextime={"歡迎您再次光臨 ~"};
//tochadj
unsigned char code *W_tochadj={"A:中獎位置調整  "};
//tochchk
unsigned char code *W_tochchk={"C:中獎位置確認  "};
//座標回復初始值 ?
unsigned char code   *W_xyinit={"座標回復初始值 ?"};
//這個位置最準嗎?
unsigned char code  *W_xyready={"這個位置最準嗎 ?"};
//>>NO    >>YES
unsigned char code     *Wnoyes={">>NO    >>YES   "};
//>>YES   >>NO    
unsigned char code     *Wyesno={">>YES   >>NO    "};
//電眼偵測中.. /-Sensor Check-
unsigned char code  *Wsensor_test={"電眼偵測中..    "};
//~ OK ~
unsigned char code   *w_ok={"    ~~ OK ~~    "};
//~ NG ~
unsigned char code   *w_ng={"    !! NG !!    "};

//~ OK ~
unsigned char code     *wchk_ok={"~ OK ~請重新開機"};

//o免費遊戲
unsigned char code         *WD2={">>免費遊戲      "};
//o機台耐久測試
unsigned char code         *WD3={">>機台耐久測試  "};
//start->PUSH 'ST'
unsigned char code     *Wpsh_st={"開始->請按鈕開始"};
//遊戲    秒後開始
unsigned char code     *Wsec_st={"遊戲    秒後開始"};

//>MOVE--> Adj. 
unsigned char code    *Wmov_adj={">>移動搖桿->調整"};

//>>PUSH-->To Game
unsigned char code    *Wpsh_togame={">>按鈕->開始遊戲"};
//>>PUSH-->RE Adj.
unsigned char code    *Wpsh_Re_adj={">>按鈕->請重調整"};

//~ THANK YOU ~
//unsigned char code *w_thank_you={"~~ THANK YOU ~~ "};
//Machine init:
unsigned char code    *wmach_init={"機台初始化....  "};

//>A:基本設定 / Setting
unsigned char code        *WSA={">>A:基本設定    "};
//>B : Payout Setting
unsigned char code        *WSB={">>B:禮品售價    "};
//>C : Accounts
unsigned char code        *WSC={">>C:查帳        "};
//>D :Play Mode
unsigned char code        *WSD={">>D:遊戲模式    "};
//>E :測試
unsigned char code        *WSE={">>E:測試        "};
//>A1投幣器設定 / Coin-->Play  
unsigned char code       *WSA1={">A1:投幣器設定  "};
//>A2 DBA設定 / DBA-->Play  
unsigned char code       *WSA2={">A2:中獎模式    "};	
//>A3 選物累加功能  
unsigned char code       *WSA3={">A3:選物累加設定"};
//>A4遊戲時間 / Play time
unsigned char code       *WSA4={">A4:遊戲時間    "};	
//>A5展示音樂 / Attract Music
unsigned char code       *WSA5={">A5:待機音樂    "};	
//>A6投幣儲存 / Coin Save/Clear
unsigned char code       *WSA6={">A6:投幣儲存    "};	
//>WSA7:CAPSULE
unsigned char code       *WSA7={">A7:干擾數值微調"};
//>WSA8:Volume Adj.
unsigned char code       *WSA8={">A8:音量調整    "};
//>WA3Q1: Dispenser ON/OFF
unsigned char code      *WSA71={"Dispenser:      "};	//ON/OFF
//>WA3Q2: Cap. Err YES/NO
unsigned char code      *WSA72={"Cap. Error:     "};	//YES/NO

//>
unsigned char code      *WSA61={"目前= 儲存      "};	//ON/OFF
//>
unsigned char code      *WSA62={"押按鈕 2秒歸零  "};	//
//>
unsigned char code      *WSA91={"= ON            "};	//ON/OFF
//>
unsigned char code      *WSA92={"= OFF           "};	//

//  Coin  Play 
unsigned char code     *WSA10={" 1投=       元  "};	
//  Coin  Play 
unsigned char code     *WSA11={"每局=       元  "};
//  Pulse  Play 
unsigned char code     *WSA21={"  Pulse=  Play  "};

//目前=  分鐘一次/  
unsigned char code     *WSA5A={"=       分鐘一次"};	
//目前=
unsigned char code     *WSA42={"=               "};	
//=   Seconds
unsigned char code    *WSA42A={"=         秒    "};	

unsigned char code     *WSA221={"目前=   固定    "};
unsigned char code     *WSA222={"目前=   促銷    "};
unsigned char code     *WSA223={"亂數    次內 1次"};
unsigned char code     *WSA224={"已累積  次; 未出"};	
unsigned char code    *WSA224A={"已累積  次; 已出"};
//  >>開啟>>顯示: 有 
unsigned char code     *WSA31={">>開啟>>顯示: 有"};	
//  >>累加永久保留
unsigned char code     *WSA32={"累加保留: 永久  "};
//  >>顯示保留  分鐘
unsigned char code     *WSA33={"顯示保留:   分鐘"};
//  >>顯示保留  永久
unsigned char code    *WSA33A={"顯示保留:   永久"};

//=   Seconds
unsigned char code    *WSA42B={"全部 =          "};		
//volume=
unsigned char code     *WSA51={"音量= +0        "};		
	
//>C1: 
unsigned char code       *WSC1={">>C1累加銀行    "};	
//>C2: 
unsigned char code       *WSC2={">>C2投幣遊戲次數"};
//>C3: 
unsigned char code       *WSC3={">>C3總出獎次數  "};		
//>C1: 
unsigned char code      *WSC10={"初始化      歸零"};
//>C1: 
unsigned char code      *WSC11={"設定        歸零"};
//>C1: 
unsigned char code      *WSC12={"中獎        歸零"};
//>C1: 
unsigned char code      *WSC13={"不正常關機  歸零"};
//>C3: 
unsigned char code      *WSC31={"不正常關機    次"};	


//>D1: 
unsigned char code       *WSD1={"==營業模式      "};	
//>D2: 
unsigned char code       *WSD2={"==免費遊戲模式  "};	
//>D3: 
unsigned char code       *WSD3={"==機台測試模式  "};	

//CA4> Reset Data
unsigned char code     *WSYES={"->YES           "};
//CA4> Reset Data
unsigned char code      *WSNO={"->NO            "};	
//數據儲存中..
unsigned char code     *Wsave={"資料儲存中....  "};

//>E1: 
unsigned char code      *WSE1={">>E1落物電眼測試"};
//>E2: 
unsigned char code      *WSE2={">>E2中獎燈測試  "};

//>E1: 
unsigned char code     *WSE11={">>可調整電眼VR  "};
//>E2: 
unsigned char code     *WSE12={"請檢查接線或電眼"};
//>E3: 
unsigned char code     *WSE13={">>按取物鈕離開  "};

//>E1: 
unsigned char code     *WSE21={">>按鈕開啟中獎燈"};
//>E2: 
unsigned char code     *WSE22={">>按鈕關閉中獎燈"};
//>E3: 
unsigned char code     *WSE23={"按小卡模式->切換"};
//>E4: 
unsigned char code     *WSE24={"<<搖桿向左<-離開"};


unsigned char code      *WB13={"目前=           "};

//^加;!減;o出 
unsigned char code WB12[16]=
{0xa1,0xf4,0xa5,0x5b,0xa1,0x51,0xa1,0xf5,0xb4,0xee,0xa1,0x51,0xa1,0xb7,0xa5,0x58};
//^!變更; o出 
unsigned char code WA110[16]=
{0x18,0x19,0xc5,0xdc,0xa7,0xf3,0xa1,0x51,0xa1,0xb7,0xa5,0x58,0x20,0x20,0x20,0x20};
//^!加減; ->右->下 
unsigned char code WA111[16]=
{0x18,0x19,0xa5,0x5b,0xb4,0xee,0xa1,0x51,0xa1,0xf7,0xa5,0x6b,0xa1,0xf7,0xa4,0x55};	
//^!加減; ->右移 
unsigned char code WA119[16]=
{0x18,0x19,0xa5,0x5b,0xb4,0xee,0xa1,0x51,0xa1,0xf7,0xa5,0x6b,0xb2,0xbe,0x20,0x20};	

//o長按歸零;<-出
unsigned char code WC20[16]=		
{0xa1,0xb7,0xaa,0xf8,0xab,0xf6,0xc2,0x6b,0xb9,0x73,0xa1,0x51,0x3c,0x2d,0xa5,0x58};
//space
unsigned char code Wspace[16]=
{0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20};	


//[機台故障]
//unsigned char code *Werror={"Machine Error:  "};
unsigned char code *Werror={"機台故障:       "};
//故障號碼:01
//unsigned char code *Werror_cd={"Error No:       "};
unsigned char code *Werror_cd={"故障號碼:       "};
//入表1故障(01)
//unsigned char code *Werror_01={"Coin1 counter   "};
unsigned char code *Werror_01={"入表1 故障      "};
//unsigned char code *Werror_offline={"Counter off line"};
unsigned char code *Werror_offline={"計數表接線不通  "};
//入表2故障(02)
//unsigned char code *Werror_02={"Coin2 counter   "};
unsigned char code *Werror_02={"入表2 故障      "};
//出表故障(03)
//unsigned char code *Werror_03={"gift out counter"};
unsigned char code *Werror_03={"出表故障        "};
//投幣器1故障(04)
//unsigned char code *Werror_04={"Coin1 NC error  "};
unsigned char code *Werror_04={"投幣器1 NC故障  "};
//投幣器2故障(05)
//unsigned char code *Werror_05={"Coin2 NC error  "};
unsigned char code *Werror_05={"投幣器2 NC故障  "};
//請注意SW:NC->NO
//unsigned char code *Werror_05a={"Check SW NC->NO "};
unsigned char code *Werror_05a={"請檢查SW:NC->NO"};
//落物電眼偵測故障(09)
//unsigned char code  *Werror_06={"Gift Sensor err."};
unsigned char code  *Werror_06={"禮品電眼偵測故障"};
//請調整電眼VR
//unsigned char code *Werror_06a={"Check Sensor VR "};
unsigned char code *Werror_06a={"請調整電眼VR    "};

//機台異常出獎
//unsigned char code  *Werror_07={"Bank Negative   "};
unsigned char code  *Werror_07={"機台異常出獎    "};
//請重新校正機台
//unsigned char code *Werror_07a={"Readjust machine"};
unsigned char code *Werror_07a={"請重新校正機台  "};
//主機板momery error
//unsigned char code  *Werror_08={"PCB Memory error"};
unsigned char code  *Werror_08={"主機板記憶體異常"};
//請送修主機板
//unsigned char code *Werror_08a={"Repair Main PCB "};
unsigned char code *Werror_08a={"請送修主機板    "};

//定位資料錯誤(10)
//unsigned char code  *Werror_09={"X.Y Pos.data err"};
unsigned char code  *Werror_09={"X.Y 定位資料錯誤"};
//請校正
//unsigned char code *Werror_09a={"Calibration >X.Y"};
unsigned char code *Werror_09a={"請重新校正>>X.Y "};
//x軸歸原點異常(10)
//unsigned char code  *Werror_10={"X. Origin sensor"};
unsigned char code  *Werror_10={"X 軸歸原點異常  "};
//x軸左限異常(11)
//unsigned char code  *Werror_11={"X. X1 sensor Err"};
unsigned char code  *Werror_11={"          異常  "};
//x軸右限異常(12)
//unsigned char code  *Werror_12={"X. Right sensor "};
unsigned char code  *Werror_12={"X 軸右極限異常  "};
//x軸右限異常(13)
//unsigned char code  *Werror_13={"X. clock sensor "};
unsigned char code  *Werror_13={"X 軸馬達感應器NG"};
//y軸歸原點異常(14)
//unsigned char code  *Werror_14={"Y. Origin sensor"};
unsigned char code  *Werror_14={"Y 軸歸原點異常  "};
//y軸上限異常(15)
//unsigned char code  *Werror_15={"Y. Up limit Err."};
unsigned char code  *Werror_15={"Y 軸往前極限異常"};
//z軸上異常(16)
//unsigned char code  *Werror_16={"Z. Top sensor   "};
unsigned char code  *Werror_16={"Z 軸歸原點異常  "};
//z軸失敗點異常(17)
//unsigned char code  *Werror_17={"Z. Fail sensor  "};
unsigned char code  *Werror_17={"Z 軸不中獎點異常"};
//z軸中獎點異常(18)
//unsigned char code  *Werror_18={"Z. Win sensor   "};
unsigned char code  *Werror_18={"Z 軸中獎點異常  "};
unsigned char code  *Werror_19={"小飛球異常      "};
// /QC test:
//unsigned char code      *Wqctest={"   QC test:     "};
unsigned char code      *Wqctest={"機台QC測試:     "};		
//A:SOUND
//unsigned char code        *WQA={">>A:sound       "};
unsigned char code        *WQA={">>A:聲音IC      "};
//B:sensor+sw
//unsigned char code        *WQB={">>B:NO FUNCTION "};
unsigned char code        *WQB={">>B:預留        "};
//>C : x軸
//unsigned char code        *WQC={">>C: >>X <<     "};
unsigned char code        *WQC={">>C: X  軸      "};
//>D : Y軸
//unsigned char code        *WQD={">>D: >>Y <<     "};
unsigned char code        *WQD={">>D: Y  軸      "};
//>E : Z軸
unsigned char code        *WQE={">>E: Z  軸      "};

//A:SOUND
unsigned char code      *WQA11={"PLAY區        首"};
//A:SOUND
unsigned char code      *WQA12={"左->離開  右->換"};
//A:SOUND
unsigned char code      *WQA13={"鈕->播  上下->播"};

//	B
unsigned char code      *WQB11={"A:X B:X L:X     "};
//B
unsigned char code      *WQB12={"1:X 2:X 3:X     "};
//B
unsigned char code      *WQB13={"左  ->離開      "};

//C
unsigned char code      *WQC11={"左 :X     右 :X "};
//C
unsigned char code      *WQC12={"X_pos:          "};
//c
unsigned char code      *WQC13={"按按鈕->離開    "};
//D
unsigned char code      *WQD11={"Y_原點 :X       "};
//D
unsigned char code      *WQD12={"Y_pos:          "};
//D
unsigned char code      *WQD13={"鈕->開始  左->離"};


//E
unsigned char code      *WQE11={"上:X  下:X      "};
//E
unsigned char code      *WQE12={"上下->馬達上下  "};
unsigned char code     *WQE12B={"按鈕調整前後左右"};

//>可調整電眼VR / Adj. Sensor VR.
unsigned char code       *WQI2={">>Sensor VR Adj."};
//請檢查接線或電眼 / Check Sensor(wire)
unsigned char code       *WQI3={"Check SensorWire"};

unsigned char code       *WQI4={"LED=OFF         "};
unsigned char code       *WQI5={"LED=ON          "};
//>I Gift Sensor
unsigned char code      *WQI22={"I2:Gift Sensor-2"};

unsigned char code       *WOK={"   << OK >>      "};
unsigned char code       *WNG={"   << NG >>      "};

//X org=
unsigned char code      *Wxorg={"X org=          "};
//Y org=
unsigned char code      *Wyorg={"Y org=          "};
//X 1-1=
unsigned char code      *Wxpp1={"X 1-1=          "};
//X 1-2=
unsigned char code      *Wxpp2={"X 1-2=          "};

//X org=
unsigned char code   *Wxyztest={"X  Y  Z  TEST   "};

//X . Y  Adj.
unsigned char code    *Wxy_adj={"X . Y Adj.<  >  "};
//X . Y  Adj.
unsigned char code    *Wxy_chk={"X_Ychk.         "};
//Joystick --> A1 
unsigned char code     *Wjoy_to={"Joystick --> A1 "};
//center --> hole 
unsigned char code  *Wcenter_ho={"Center --> hole "};
//Push --> 'ST'
unsigned char code    *Wpush_st={"Push --> 'Start'"};

