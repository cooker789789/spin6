/********************************************/
/** 機種: 穿梭II                    **/
/** 日期: 12-11-2017                       **/
/** 內容:主程式 .. SPIN2-V01.C                  **/

/** Designer: WANG SHIH CHANG              *
/password **0002;0003;0004;0005;0006=random; 0007= 累加金額歸零時機:00=初始化/ 01=設定 ;/ 02=中獎;/ 03=不正常關機 ; 0008=不正常關機    次;0009=chk;
/**0010=0 & 1 error09定位資料錯誤，請重新定位; ok=0x99   ;0012volume_M  ;001f:=0x1f 中獎中途被關機，下一次開機清除資料
/**0020=credit;0021=credit;0022=playtime_M ;0023= attmusic_M(1~30) ,0xff=off;0024=catchcoin_M;0025=freeplay_M; 0029=cd_check ;002a=TSno_M;002b=win_modM;002c=TS_out_M;002d=TSnowNO;002e=bo_adjM
/** 0030=coin1_set;0031=coin1_play;0032=coin1fp_M;0033=coin1_chk;0034=coin2_set;0035=coin2_play;0036=coin2fp_M;0037=coin_chk;0038=coin_en;0039=capsule_M;003a 3b=ball_credit
;003d=已出球記憶
/**0040,0041=gmoney_M;0042=gmoney_chk;;0043=gmoneyHoldTime_M;0044=gmoneyDspTime_M;0045=gmoneyEn_M;0046gmoneyTime_chk
/**0060,0061=payout_M[0];0062=chk;0063,0064=payout_M[1];0065=chk;0066,0067=payout_M[1];0068=chk;
/**0070,0071=coin_play_M[0],0072=_chk;0073,0074=coin_play_M[1],0075=_chk;0076,0077=coin_play_M[1],0078=_chk
/**0080,0081=gift_out_M[0],0082=_chk;0083,0084=gift_out_M[1],0085=_chk;0086,0087=gift_out_M[1],0088=_chk
/**0090,0091=game_bank_M[0],0092=_chk;0093,0094=game_bank_M[1],0095=_chk;0096,0097=game_bank_M[2],0098=_chk
/**定位資料
lineX[3][5]:
0200~0202:lineX[0][0], 0203~0205:lineX[0][1],  0206~0208:lineX[0][2],  0209~020b:lineX[0][3],  020c~020e:lineX[0][4]
//0210~0212:lineX[0][5], 0213~0215:lineX[0][6],  0216~0218:lineX[0][7],  0219~021b:lineX[0][8],  
0220~0222:lineX[1][0], 0223~0225:lineX[1][1],  0226~0228:lineX[1][2],  0229~022b:lineX[1][3],  022c~022e:lineX[1][4]
//0230~0232:lineX[1][5], 0233~0235:lineX[1][6],  0236~0238:lineX[1][7],  0239~023b:lineX[1][8],  
0240~0242:lineX[2][0], 0243~0245:lineX[2][1],  0246~0248:lineX[2][2],  0249~024b:lineX[2][3],  024c~024e:lineX[2][4]
//0250~0252:lineX[2][5], 0253~0255:lineX[2][6],  0256~0258:lineX[2][7],  0259~025b:lineX[2][8],  

lineY[3][5]:
0300~0302:lineY[0][0], 0303~0305:lineY[0][1],  0306~0308:lineY[0][2],  0309~030b:lineY[0][3],  030c~030e:lineY[0][4]
//0310~0312:lineY[0][5], 0313~0315:lineY[0][6],  0316~0318:lineY[0][7],  0319~031b:lineY[0][8],  
0320~0322:lineY[1][0], 0323~0325:lineY[1][1],  0326~0328:lineY[1][2],  0329~032b:lineY[1][3],  032c~032e:lineY[1][4]
//0330~0332:lineY[1][5], 0333~0335:lineY[1][6],  0336~0338:lineY[1][7],  0339~033b:lineY[1][8],  
0340~0342:lineY[2][0], 0343~0345:lineY[2][1],  0346~0348:lineY[2][2],  0349~034b:lineY[2][3],  034c~034e:lineY[2][4]
//0350~0352:lineY[2][5], 0353~0355:lineY[2][6],  0356~0358:lineY[2][7],  0359~035b:lineY[2][8],  



SPIN2-V01
X & Y 同時回歸
SPIN2-V02
Y 先回歸，X在回歸
SPIN2-V03
計算 Y每步=0.056mm, X每步=1.25mm 
x干擾值+-7步;x干擾值+-50步 ; 2/0.056=35
SPIN2-V03t 座標直接輸入耐久測
SPIN2-V04 中獎率
SPIN2-V05  X+2,Y-320
SPIN2-V10 adj 
新版:短按自動縮回，每一格可獨立校正，有自己的x,y值
SPIN2-A01 中文選物版本 
中文選物版本 禮品孔位加大，免費遊戲不干擾
加設定 A2 中獎模式
F18D-A01 31吋 蜜蜜熊
F18D-A01 確認失敗，可以直接重調
增加查詢促銷次數，與已出/未出狀態
F18D-A02
Y干擾值50
/********************************************/
#include <REG_MG82FL524_564.h>
#include <TRUMANIO.h>
#include <lcm_spin2CH.h> //英文字型檔
//#include <delay16m.c>	
#define REDATA_TH0	0xF8; // 1ms  1000/0.543=1842
#define REDATA_TL0	0xce; /* TL0 */	
#define REDATA_TH1	0xdc; // 5ms  5000/0.543=9208 
#define REDATA_TL1	0x08; /*  */
#define PASS1	0x45; /*  */
#define PASS2	0x47; /*  */
#define PASS3	0xAB; /*  */
#define PASS4	0xAC; /*  */
#define		ISP_ENABLE		0x80
#define		ISP_DISABLE		0x00
	
//void fm25cmd(unsigned char i);
void fm25writ_data8(unsigned int k,unsigned char j);	
void fm25writ_data8tr(unsigned int k,unsigned char j);
unsigned char fm25read_data8(unsigned int k);
void fm25writ_data16tr(unsigned int k,unsigned int j);
void fm25writ_data16(unsigned int k,unsigned int j);	
unsigned int fm25read_data16(unsigned int k);

unsigned char data ot_display=0,ot_relay=0,ot_jplight=0,ot_meter=0,ot_dip=0,sec_time=0,sec_time2=0,tep_t1=0,tep_t2=0,tep_ts=0;
unsigned char data coin1_time=0,coin2_time=0,random,rand,cwno=0,Y_gost;
unsigned char playtime,coin1meter=0,coin2meter=0,et1_err=0,outmeter=0,com1_time=0,com2_time=0,outm_time=0,play;
unsigned char playtime_M,attmusic_M,freeplay_M,mtime,bo_adjM,demoledt,demoledn;	
unsigned char coin1_play,coin1_set,attmusic_min,gmoneyDspTime_M;
unsigned char coin_test,volume_M,catchcoin_M,meter_n[3],ckpush,ffd,ffb,RLf;
unsigned char play_X,play_Y,winlightsec,TSno_M,win_modM,TS_out_M,TSnowNO;
unsigned int data tep_t10=0,tep_t11=0,x_pos=0,y_pos=0,credit_int=0;

unsigned int coinplayCount_M,payout_M,lineX[3][5],lineY[3][5],coin_play_M,gift_out_M,gmoney_M,gmoneydsptsec;
signed int game_bank_M;
unsigned char pdata hexdec[5];
//unsigned char pdata lcm_credit[5]={0xfe,0x45,0x51,
bit coin_enb=0,bcoin1_h=0,bcoin2_h=0,bcoin1_m=0,bcoin2_m=0,out_m=0;
bit sec_b=0,coin_testb=0,playsb=0,st_lightb=0,sps=0,bank_add_b=0,bank_fp_b=0;
bit free_play_b=0,test_play_b=0,coinsp=0;
bit Xcountb=0,bsa=0,bsb=0,bsc=0,demoledb=0;
bit push_off_b=0,check_push_b=0,win_b=0;
bit gift_sensor_onb=0,gift_sensor_chk=0,winlightb=0;
bit gmoney_addb=0,gift_money_okb=0,gift_dsp_b=0,gift_time_b=0,play_pushb=0;
//unsigned char code seg7date[30]=
//{0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7C,0x07,0x7F,0x67,0x00,0x78,0x73,0x40,0x00,0x71,0x77,0x7C,0x39,0x5e,0x79,0x71,0x18,0x0c,0x1c,0x23,0x58,0x4c,0x3e,0x38};
//程式版本
unsigned char code   *Wtitle_1={"**  F18D-A02  **"}; //16	
unsigned int code Y_MAX=5800;//	6600
unsigned int code X1_init=21; //x 初值 step 27
unsigned char code X_xstep=38;////x 間隔 step 58
unsigned char code X_2xstep=82;  //116
unsigned int code Y1_init=1066;//y 初值 step1263
unsigned int code Y_ystep=2284;////y 間隔 step 2455
unsigned int code Y1_check=700;//y 初值 未超過此值即縮回
//unsigned int code Y_2ystep=3840;
//unsigned int code Xplay_step[9]={65,123,181,239,297,355,413,471,529}; // X 定位點初值
//unsigned int code Yplay_step[3]={1210,3130,5050}; //Y 定位點初值

//unsigned int code Xplay_step[3][9]=
//{65,123,181,239,297,355,413,471,529,
//65,123,181,239,297,355,413,471,529,
//65,123,181,239,297,355,413,471,529};

//unsigned int code Yplay_step[3][9]=
//{1210,1210,1210,1210,1210,1210,1210,1210,1210,
//3130,
//5050}; 

unsigned char code X_gost=6;//x干擾值
unsigned char code Y_gost0=50;//Y干擾值


unsigned char code Y_Playstep=30;

void AUXRA_Write(unsigned char Data)
{ 
	IFD = Data;
	IFMT = 0x06;
	ISPCR = ISP_ENABLE;
	SCMD = 0x46;
	SCMD = 0xB9;
	ISPCR = ISP_DISABLE;
}

unsigned char AUXRA_Read(void)
{
	unsigned char	Data;
    IFADRH = 0x00;
    IFMT = 0x07;
	ISPCR = ISP_ENABLE;
    SCMD = 0x46;
    SCMD = 0xB9;
    ISPCR = ISP_DISABLE;
	Data = IFD;
	return Data;
}
void ic_6295_ch4(void)
{
   unsigned char i;
   	VOICE_6295=0x40;		//ch4=0x40
	for(i=0;i<=210;i++);
	for(i=0;i<=210;i++);
	VOICE_6295=0x81;			//voice no. out		
	for(i=0;i<=210;i++);
	for(i=0;i<=210;i++);
	i=10-volume_M;
	VOICE_6295=(0x80|i);		//ch4=0x80
	//for(i=0;i<=100;i++);
}   

void timer0(void) interrupt 1 using 1
{
  unsigned char i,j;	
	i=INA000;
	j=INC000;
	if((j&0x08)==0){ //a
		bsa=1;
		if(cwno==3){
			if(Xcountb==1)
					x_pos++;
		}
		else if(cwno==5){
			if(Xcountb==1){
				 if(x_pos>0)
					x_pos--;
			}
		}
		cwno=1;
	}
	else
		bsa=0;
	if((i&0x80)==0)
		bsb=1;
	else
		bsb=0;
	if((i&0x40)==0)
		bsc=1;
	else
		bsc=0;
	
	if(cwno==1 && bsb==1){
		cwno=2; //cw-2
	}
	else if(cwno==2 && bsc==1){
		cwno=3; //cw-3
	}
	else if(cwno==1 && bsc==1){
		cwno=4; //ccw-2
	}
	else if(cwno==4 && bsb==1){
		cwno=5; //ccw-3
	}
	
	
 TL0 = REDATA_TL0;
 TH0 = REDATA_TH0;   
}
void timer1(void) interrupt 3 using 2
{
unsigned char i;     
//亂數運算
  if(tep_ts==0){
  		  tep_ts=1;
  		  rand=(random&0x80)>>7;
  }
  else if(tep_ts==1){
  		  tep_ts=2;
  		  rand^=(random&0x08)>>3;
  }		  
  else if(tep_ts==2){
  		  tep_ts=3;
  		  rand^=(random&0x04)>>2;
  }			  
  else if(tep_ts==3){
  		  tep_ts=4;
  		  rand^=(random&0x02)>>1;
  }	
  else if(tep_ts==4){
  		  tep_ts=0;
  		  random<<=1;
  		  random|=rand;
  }	
  else;
  		  
  
  if(tep_t1!=0)
  	tep_t1--;	  
  if(tep_t2!=0)
  	tep_t2--;
  
  if(tep_t10!=0)
  			tep_t10--;	  
  if(tep_t11!=0)
  			tep_t11--;	
  sec_time++;
  if(sec_time==200){
  		sec_time=0;
  		attmusic_min++;
  		if(playsb==1){
  			if(playtime!=0){
  				playtime--;
  				sec_b=1;
  			}
  		}
  		if(gift_time_b==1){		//選務顯示時間倒數
  			if(gmoneydsptsec!=0)
  				gmoneydsptsec--;
  			else{
  					gift_time_b=0;
  					gift_dsp_b=0; // 不顯示
  			}		
  		}
  		if(winlightb==1){
  			winlightsec--;
  			if(winlightsec==0){
  				winlightb=0;
  				ot_meter&=0xdf; //中獎燈 off
  			}
  		
        }
  		
  }
  if(sec_time==100 || sec_time==0 ){ //0.5秒
  		 sps=~sps; 
  		 if(st_lightb!=0){
  		 	 if(sps==1)
  				   ot_meter|=0x08; //開始燈
  				else
  				   ot_meter&=0xf7;	
  		}
  }
 sec_time2++;
  if(sec_time2==200){
  		sec_time2=0;
  		if(mtime!=0)
  			mtime--;
  }		
  
 		 
 if(et1_err==0 && coin_enb==1){
 	 i=INC000;
    if(bcoin1_h==1){
         coin1_time++;
      if(coin1_time==100){
        ot_meter&=0x7f;//coin power off
        et1_err=0x04;	//COIN  1 ERROR
        bcoin1_h=0;
      }  	  
      else if((i&0x20)!=0){
		bcoin1_h=0;
		if(coin1_time>3){
			if( coin_testb==0){	//不在測試模式	
          		coin1meter++;
  		  		credit_int+=coin1_set;
  				//play=credit_int;
  				coinsp=1;
  			}
  			else coin_test++;	
        	ic_6295_ch4();			//coin voice
        }
  		 coin1_time=0; 
       }    
       else;
   }
   else if(((i&0x20)==0)&& (bcoin1_h==0)  ) bcoin1_h=1; //
           
   else; 
   
  } //if(et1_err==0)
  	  
  	  
 if(bcoin1_m==1){		//coin1 跳表
   com1_time++;
   if(com1_time==12){
     ot_meter&=0xfe;		/*coin1 meter off*/
     bcoin1_m=0;
   }  
 }
 else if(bcoin1_m==0 && com1_time!=0)  com1_time--;
  
 else if(coin1meter!=0 && com1_time==0){ 
   bcoin1_m=1;
   ot_meter|=0x01;		/*coin1 meter on*/
   coin1meter--;
 }
 else;    
 if(bcoin2_m==1){		//coin2 跳表
   com2_time++;
   if(com2_time==12){
     ot_meter&=0xFD;		/*coin2 meter off*/
     bcoin2_m=0;
   }  
 }
 else if(bcoin2_m==0 && com2_time!=0) com2_time--;
  
 else if(coin2meter!=0 && com2_time==0){ 
   bcoin2_m=1;
   ot_meter|=0x02;		/*coin2 meter on*/
   coin2meter--;
 }
 else;   
 if(out_m==1){		//out 跳表
   outm_time++;
   if(outm_time==12){
     ot_meter&=0xfB;		/*out meter off*/
     out_m=0;
   }  
 }
 else if(out_m==0 && outm_time!=0) outm_time--;
  
 else if(outmeter!=0 && outm_time==0){ 
   out_m=1;
   ot_meter|=0x04;		/*out meter on*/
   outmeter--;
 }
 else;    
 //遊戲中檢查取物鈕
 if(check_push_b==1){
 	 i=INA000;
 	 if((i&0x20)!=0){
 	 	 ckpush++;
 	 	 if(ckpush>=4){
 	 	 	 check_push_b=0;
 	 	 	 push_off_b=1;
 	 	 }
 	 }
 	 else
 	 	 ckpush=0;
 	 	 	 
 }	 
 	 	 	 
  if(gift_sensor_chk==1){
 		 if(P35==0)
 		 		 gift_sensor_onb=1;
 }		 
  
   /**中獎燈閃爍**/
  if(winlightb==1){ //中獎燈閃爍 	  
 	demoledt--;
 	if(demoledt==0){
 	 	demoledt=25; 
 	 	demoledb=~demoledb;
     	if(demoledb==1){
     			ot_jplight|=0x07;//jp ON
     	}
     	else{
     			ot_jplight&=0xf8;//jp OFF
     	} 
    }
    
 }
 else{
 		demoledt--;
 		if(demoledt==0){
 	 		demoledt=250; 
 	 		demoledn++;
 	 		if(demoledn>=6)
 	 			demoledn=0;
 	 		ot_jplight&=0xf8;//jp
 	 		if(play_pushb==0){
 	 			switch(demoledn){
 	 				case 0:ot_jplight|=0x01;//jp1
 	 					break;
 	 				case 1:ot_jplight|=0x02;//jp1
 	 					break;
 	 				case 2:ot_jplight|=0x04;//jp1
 	 					break;
 	 				case 3:ot_jplight|=0x01;//jp1
 	 					   ot_jplight|=0x02;//jp1
 	 					break;
 	 				case 4:ot_jplight|=0x01;//jp1
 	 					   ot_jplight|=0x04;//jp1
 	 					break;	
 	 				case 5:ot_jplight|=0x02;//jp1
 	 					   ot_jplight|=0x04;//jp1
 	 					break;
 	 			}
 	 		}	
 	 	}
}
  
  
 OT2000=ot_display;
 OT3000=ot_relay;
 OT4000=ot_jplight;	
 OT5000=ot_meter;
 OT6000=ot_dip;	
 TL1 = REDATA_TL1;
 TH1 = REDATA_TH1;   	  
} 	  	  




void OutputInital(void)
{
   P1=0xff;	
   P3=0xff;
   P33=1;	// FM25 SO
   P10=1;  //FM25 CS
   P32=0;  //FM25 CLK
   P11=0;  //FM25 SI
   P14=1;
   P15=1;  	   	   	   
   P16=1;
   P17=1;
   	
 	P21=0;	//LCMSI
 	P20=0;	//LCMSCK
 	
 	playtime=0;
 	play=0;
 	ot_dip|=0x0c; //步進 cw & ccw=1
	OT6000=ot_dip;
 	RLf=0;
 	ffd=1;
	ffb=1;
	demoledt=1;
 	demoledn=0;
}   





/* TIMER 0 初始設定 */  
void timer_init(void)
{
        TMOD = 0x11; /* 計數器 1 MODE 1;計時器0,MOD=1 */
        TL0 = REDATA_TL0;
        TH0 = REDATA_TH0;
        TL1 = REDATA_TL1;
        TH1 = REDATA_TH1;
        /*
        S2CON=0xc0;	   //mode 13 :sm0=1 sm1=1 sm2=0 S2CON=0x60;	   //mode 13 :sm0=1 sm1=1 sm2=0 
        AUXR= 0xa0;  //URTS=1 UART1借用UART2鮑率 P41ALE	;URTS必須設定
        S2BRT=253;		//19200
        AUXR2=0x18;		//S2TR=1  start baud, S2MOD=1	
        S2CON|=0x30;//S2SM2=1	  S2REN=1   
        AUXIP=0X10;	
        AUXIPH=0X10;
        AUXIE=0x10;//ES2=1
        	*/
        //ET0=1;
        TR0=1;
        ET1=1;
        TR1=1;
       EA = 1;
}   
void delayusf(unsigned char r)
{
   unsigned char i,j;
   for(j=0;j<r;j++)
       for(i=0;i<=100;i++);
}   
void delayus(unsigned char r)
{
   unsigned char i,j;
   for(j=0;j<r;j++)
       for(i=0;i<=200;i++);
}   
  
void delay1ms(unsigned char r)
{
   unsigned char i,j,k;
   for(k=0;k<r;k++)
     for(j=0;j<16;j++)
       for(i=0;i<=135;i++);
}       
void delay100ms(unsigned char p)
{
   unsigned char i,j,k,r;
   for(r=0;r<p;r++)
    for(k=0;k<100;k++)
     for(j=0;j<16;j++)
       for(i=0;i<=135;i++);
}   
//中文液晶
void spi_data(bit rs,unsigned char a)
{
unsigned char i,j;
	//ET1=0;
	EA=0;
	//連續5個bit送1
	for(i=0;i<=4;i++){
		P20=0;
		P20=0;
		P21=1;	//sid=P21
		P21=1;	//sid=P21
		P20=1;  //clk
		P20=1;  //clk
	}	
	//RW=0	
	P20=0;
	P20=0;
	P21=0;	//sid=P21
	P21=0;	//sid=P21
	P20=1;  //clk	
	P20=1;  //clk
	//RS
	P20=0;
	P20=0;
	if(rs!=0){
 			P21=1;	//sid=P21
 			P21=1;	//sid=P21
 	}
 	else{
 			P21=0; //sid=P21
 			P21=0; //sid=P21
 	}
 	P20=1;	
 	P20=1;
	//bit8=0
	P20=0;
	P20=0;
	P21=0;	//sid=P21
	P21=0;	//sid=P21
	P20=1;  //clk	
	P20=1;  //clk
	//data D7~D4 +0000;D3~D0 +0000
	P20=0;
	j=0x80;
 	for(i=0;i<=3;i++){//data D7~D4
 		P20=0;
 		P20=0;
 		if((a&j)!=0){
 			P21=1;	//sid=P21
 			P21=1;	//sid=P21
 		}
 		else{
 			P21=0; //sid=P21
 			P21=0; //sid=P21
 		}
 		j>>=1;	
 		P20=1;
 		P20=1;
 	}
 	//連續4個bit送0
	for(i=0;i<=3;i++){
		P20=0;
		P20=0;
		P21=0;	//sid=P21
		P21=0;	//sid=P21
		P20=1;  //clk
		P20=1;  //clk
	}	
 	P20=0;
	j=0x08;
 	for(i=0;i<=3;i++){//data D3~D0
 		P20=0;
 		P20=0;
 		if((a&j)!=0){
 			P21=1;	//sid=P21
 			P21=1;	//sid=P21
 		}
 		else{
 			P21=0; //sid=P21
 			P21=0; //sid=P21
 		}
 		j>>=1;	
 		P20=1;
 		P20=1;
 	}
 	//連續4個bit送0
	for(i=0;i<=3;i++){
		P20=0;
		P20=0;
		P21=0;	//sid=P21
		P21=0;	//sid=P21
		P20=1;  //clk
		P20=1;  //clk
	}	
 	P21=0;
 	P21=0;
 	P20=0;
 	P20=0;
 	EA=1;
	//ET1=1;
	if(rs==0)
		delayus(4);	
	delayusf(1);
}	

void LCM_clr(void)
{
	spi_data(0,0x30);//function set
 	spi_data(0,0x0c); //display on/off/ cursor off
 	spi_data(0,0x01);//display clear
 	delay1ms(10);
 	spi_data(0,0x06);//entry mode
}
void W_dspace(unsigned char a)
{
 spi_data(0,a);//cursor 第1行
 spi_data(1,0x20);//
 spi_data(1,0x20);//
} 
		
void W_DISP(unsigned char a,unsigned char *ps)
{     		 
 unsigned char i,j;		
     spi_data(0,a);//cursor 第1行
 	 for(i=0;i<=15;i++){
 			j=ps[i];//
 			spi_data(1,j);//
 	}	
}
void W_DISPh(unsigned char a,unsigned char m,unsigned char n,unsigned char *ps)
{     		 
 unsigned char i,j;		
     spi_data(0,a);//cursor 第1行
 	 for(i=m;i<=n;i++){
 			j=ps[i];//
 			spi_data(1,j);//
 	}	
}
void grafic_clear(void)
{
  unsigned char i,j;
  spi_data(0,0x34);//function set RE=1
 	delayus(50);
 for(j=0;j<=31;j++){	
 	spi_data(0,0x80+j);//繪圖位址Y
 	spi_data(0,0x80);//繪圖位址X
 		for(i=0;i<=7;i++){
 			spi_data(1,0);//
 			spi_data(1,0);//
 		}	
 }		
 for(j=0;j<=31;j++){	
 	spi_data(0,0x80+j);//繪圖位址Y
 	spi_data(0,0x88);//繪圖位址X
 		for(i=0;i<=7;i++){
 			spi_data(1,0);//
 			spi_data(1,0);//
 		}	
 }			
}



       
/* 8 位元 hex 轉 bcd 碼 作顯示使用 */
unsigned char hex_dec8(unsigned char k)
{
  unsigned char i,dt1;
  
  hexdec[2]=k/100;	/*中位元 2*/
  dt1=k%100;	
  hexdec[1]=dt1/10;	/*中位元 5*/
  hexdec[0]=dt1%10;	/*低位元 5*/
  i=(hexdec[1]<<4)|hexdec[0];
  return(i);	  
  
}
void hex_dec(unsigned int k)
{
  unsigned int dt1,dt2;
  unsigned char dt3;
  hexdec[4]=k/10000;  /*高位元 6*/
  dt1=k%10000;
  hexdec[3]=dt1/1000;	/*中位元 5*/
  dt2=dt1%1000;
  hexdec[2]=dt2/100;	/*中位元 5*/
  dt3=dt2%100;	
  hexdec[1]=dt3/10;	/*中位元 3*/
  hexdec[0]=dt3%10;	/*低位元 6*/
  
}	 		

void ic_6295_ch(unsigned char chnnel,unsigned char n,unsigned char m)
{
   unsigned char i,k;
	VOICE_6295=chnnel;		//ch1=0x08,ch2=0x10,ch3=0x20,ch4=0x40
	for(i=0;i<=210;i++);
	for(i=0;i<=210;i++);
	i=0x80 | n;
	VOICE_6295=i;			//voice no. out
	for(i=0;i<=210;i++);
	for(i=0;i<=210;i++);
	chnnel<<=1;
	//m++;
	k=10-volume_M;
	k=k+m;
	chnnel|=k;			//衰減 m
	VOICE_6295=chnnel;		//ch1=0x10,ch2=0x20,ch3=0x40,ch4=0x80
		
}

void bank_6295(unsigned char n)
{
  if(n==0)	
   ot_dip&=0xef;    //vk1=0 bank0
   else
   	ot_dip|=0x10;      /*vk=1 bank1 */      
 OT6000=ot_dip;	
}


unsigned char check_6295(unsigned char chnnel)
{
  unsigned char m;
	m=VOICE_6295;
	m=m&chnnel;		//ch1=0x01,ch2=0x02,ch3=0x04,ch4=0x08
	return(m);

}
void wait_6295(unsigned char chnnel)
{
  unsigned char m;
  do{
  	  delay1ms(20);
		m=VOICE_6295;
		m=m&chnnel;		//ch1=0x01,ch2=0x02,ch3=0x04,ch4=0x08
	}while(m!=0);

}

 
void vocice_6295_stop(void)
{
	unsigned char i;
      VOICE_6295=0x78;		//chennel 1234 off
      for(i=0;i<=210;i++);
	  for(i=0;i<=210;i++);
}   
/*
void vocice_6295_stop_chnnel(unsigned char a)
{
	unsigned char i;
      VOICE_6295=a;		//chennel 1234 off
      for(i=0;i<=210;i++);
	  for(i=0;i<=210;i++);
}   */

void X_motor_off_s(void)
{
		CCAPM2=0x0; //comp=0 & pwm=0
  		PCAPWM2=0x00;  // hi BIT9=0
 		CCAP2H=0;
 		P14=1;
}
void Z_motor_off_s(void)
{
		CCAPM3=0x0; //comp=0 & pwm=0
  		PCAPWM3=0x00;  // hi BIT9=0
 		CCAP3H=0;
 		P15=1;
}
void error(unsigned char a)
{
unsigned char i,p;
//bit bb;
	coin_enb=0;
	//errorb=1;
	ot_meter&=0x7f;//coin power off
    //ot_jplight&=0x7f;//jp7 =0 bill disable
   vocice_6295_stop();
  //	disp_CoinTime();
  
  X_motor_off_s();
  Z_motor_off_s();
   LCM_clr();
   W_DISP(0x80,Werror);
   W_DISP(0x90,Werror_cd); 
   p=hex_dec8(a);
   spi_data(0,0x95);//cursor 第1行
   i=0x30+hexdec[1];
   spi_data(1,i); //
   i=0x30+hexdec[0];
   spi_data(1,i); //
   switch(a){
    				 case 1:W_DISP(0x88,Werror_01);
    				 		break;
    				 case 2:W_DISP(0x88,Werror_02);
    				 		break; 
    				 case 3:W_DISP(0x88,Werror_03);
    				 		break;
    				 case 4:W_DISP(0x88,Werror_04);
    				 		W_DISP(0x98,Werror_05a);
    				 		break;
    				 case 5:W_DISP(0x88,Werror_05);
    				 		W_DISP(0x98,Werror_05a);
    				 		break; 
    				 case 6:W_DISP(0x88,Werror_06);
    				 		W_DISP(0x98,Werror_06a);
    				 		break;
    				 case 7:W_DISP(0x88,Werror_07);
    				 		W_DISP(0x98,Werror_07a);
    				 		break;
    				 case 8:W_DISP(0x88,Werror_08);
    				 		W_DISP(0x98,Werror_08a);
    				 		break; 
    				 case 9:W_DISP(0x88,Werror_09);
    				 		break;
    				 case 10:W_DISP(0x88,Werror_10);
    				 		//W_DISP(0x98,Werror_10a);
    				 		break;
    				 case 11:W_DISP(0x88,Werror_11);
    				 		break; 
    				 case 12:W_DISP(0x88,Werror_12);
    				 		break;
    				 case 13:W_DISP(0x88,Werror_13);
    				 		break;
    				 case 14:W_DISP(0x88,Werror_14);
    				 		break; 
    				 case 15:W_DISP(0x88,Werror_15);
    				 		break;		
    				 case 16:W_DISP(0x88,Werror_16);
    				 		break;
    				 case 17:W_DISP(0x88,Werror_17);
    				 		break;
    				 case 18:W_DISP(0x88,Werror_18);
    				 		break;
    				 case 19:W_DISP(0x88,Werror_19);
    				 		break;		
    }	 
    
  bank_6295(1); 
 while(1){
  ic_6295_ch(0x08,7,0);   //click1 error
  delay100ms(7);
 //delay100ms(7);
  
 }	
		
}

void voice_dodo(unsigned char a)
{
 unsigned char i;
   bank_6295(0); 
   for(i=0;i<a;i++){
	ic_6295_ch(0x08,2,0);   //click1 error
  	delay100ms(3);
  }	
}
void save_data(void)
{
    LCM_clr();
	W_DISP(0x90,Wsave);
    voice_dodo(1);
    delay100ms(5);
}
void Ramint_save(unsigned int p,unsigned int h)
{
 unsigned char i;	
	fm25writ_data16(p,h);
	
	i=((h>>8)&0x00ff)^(h&0x00ff)^0x55;
		fm25writ_data8(p+2,i);
}	
void coinplay1_set_save(void)
{		
 unsigned char a;		
	fm25writ_data8(0x0031,coin1_play); 
	fm25writ_data8(0x0032,coin1_set); 
	a=coin1_play^coin1_set^0x55;
 	fm25writ_data8(0x0033,a);
}
void coin1_data_init(void)
{
	coin1_set=10;		//1投幾元
 	coin1_play=10;		//1PLAY= 幾元
	coinplay1_set_save();
}
unsigned char coin1_data_out(void)
{		
	unsigned char a,c;	
 	coin1_play=fm25read_data8(0x0031);
 	coin1_set=fm25read_data8(0x0032);
 	a=fm25read_data8(0x0033);
 	c=coin1_play^coin1_set^0x55;
 	if(a==c)
 		return(0);
 	else
 		return(1);
}	

void playtime_data_init(void)
{
 	playtime_M=30;
 	fm25writ_data8(0x0022,playtime_M);
}
void attmusic_data_init(void)
{
	attmusic_M=5;
 	fm25writ_data8(0x0023,attmusic_M);
 	
}
void data8_ram_save(unsigned int m,unsigned char n)
{		
 
 		fm25writ_data8(m,n);
		n=n^0x55;
 		fm25writ_data8(m+1,n);
}	
unsigned char data8_ram_read(unsigned char n) 
{
	unsigned char i,j;
	i=fm25read_data8(n);
	j=fm25read_data8(n+1);
	if(j==(i^0x55))
		return(i);
	else{
		data8_ram_save(n,0);
		return(0);
	}
		
		
}



void payoutM_init(void)
{		
	payout_M=300;
	Ramint_save(0x0060,payout_M);
	
}
void coin_playM_0(void)
{		
	coin_play_M=0;
	Ramint_save(0x0070,0);
}
void gift_outM_0(void)
{		
	gift_out_M=0;
	Ramint_save(0x0080,0);
	
}

void game_bankM_0(void)
{
	game_bank_M=0;
	Ramint_save(0x0090,0);
	
}

void booking_init(void)
{		
  coin_playM_0();
  gift_outM_0();
  game_bankM_0();
  
}

void payoutM_load(void)
{		
  unsigned char j,n,k,m;
  unsigned int h;
  
  m=0x50;
  for(k=0;k<=3;k++){ //0x60;0x70;0x80;0x90
  	   		m+=0x10;
  	   
			h=fm25read_data16(m);
  			j=fm25read_data8(m+2);
  			n=((h>>8)&0x00ff)^(h&0x00ff)^0x55;
  			if(n!=j){
  				if(k==0)
  					payoutM_init(); //0x60
  				else if(k==1)
  					coin_playM_0(); //0x70
  				else if(k==2)
  					gift_outM_0(); //0x80	
  				else 
  					game_bankM_0(); //0x90	
  				break;
  			}
  			else{
  				if(k==0){
  					if(h==0)
  						payoutM_init(); //0x60
  					else	
  						payout_M=h;
  				}
  				else if(k==1)
  					coin_play_M=h;
  				else if(k==2)
  					gift_out_M=h;
  				else
  					game_bank_M=h;
  			}
  }
  		
}
unsigned char gmoney_load(void)
{		
 unsigned char a,k,i;		
    gmoney_M=fm25read_data16(0x0040);
 	i=fm25read_data8(0x0042);
 	a=(gmoney_M>>8)&0x00ff;
 	k=gmoney_M&0x00ff;
 	a=a^k^0x55;
 	return(a^i);
} 	

	
void gmoney_00(void)
{
 	gmoney_M=0;	//累加金額
	Ramint_save(0x0040,gmoney_M);
}
void gmoney_data_init(void)
{
	gmoney_00();////累加金額歸零
	fm25writ_data8(0x0007,0x00);//初始化=00
	gmoneyDspTime_M=3; //顯示時間
	fm25writ_data8(0x0044,gmoneyDspTime_M);
	
}


void data_init_in(void)
{
unsigned char i,j;
	i=PASS1;		
	fm25writ_data8(0x0002,i);
	i=PASS2;		
	fm25writ_data8(0x0003,i);
	i=PASS3;		
	fm25writ_data8(0x0004,i);	
    i=PASS4;		
	fm25writ_data8(0x0005,i);
	coin1_data_init();
	//coin2_data_init();
	playtime_data_init();
	coinplayCount_M=0;
	Ramint_save(0x0066,0);
	attmusic_data_init();
	catchcoin_M=0;	// coin_save=clear
	fm25writ_data8(0x0024,catchcoin_M);
	freeplay_M=0;
	data8_ram_save(0x0025,freeplay_M);
 	volume_M=5;
 	fm25writ_data8(0x0012,volume_M);
 	TSno_M=50;
 	fm25writ_data8(0x002a,TSno_M);
 	win_modM=0;
 	fm25writ_data8(0x002b,win_modM);
 	bo_adjM=0;//干擾補正值
   	fm25writ_data8(0x002e,bo_adjM);
 	booking_init();
 	payoutM_init();
 	gmoney_data_init();
 	
	i=fm25read_data8(0x0002);
	j=PASS1;
	if(i==j){
			i=fm25read_data8(0x0003);
			j=PASS2;
			if(i==j){
				i=fm25read_data8(0x0004);
				j=PASS3;
				if(i==j){
					i=fm25read_data8(0x0005);
					j=PASS4;
					if(i!=j)
							error(8);
				}
				else error(8);
			}
			else error(8);		
	}
	else error(8);	
	
	
}
void check_ram_init(void)
{
	unsigned char i,j;	
	bit b;
	b=0;
	i=fm25read_data8(0x0002);
	j=PASS1;
	if(i==j){
			i=fm25read_data8(0x0003);
			j=PASS2;
			if(i==j){
				i=fm25read_data8(0x0004);
				j=PASS3;
				if(i==j){
					i=fm25read_data8(0x0005);
					j=PASS4;
					if(i!=j)
							b=1;
				}
				else b=1;
			}
			else b=1;		
	}
	else b=1;	
	if(b==1)
		 data_init_in();	
	
}

//回復0= OK ; 1= 需定位，資料不回初值; 2= 需定位，資料回初值 
unsigned char lineXY_load(void)
{
unsigned char i,j,k,m,n;
unsigned int h,hn;	

  h=0x0200;
  for(n=0;n<=1;n++){
  	  if(n!=0)
		h=0x0300;
	  for(i=0;i<=2;i++){
			for(j=0;j<=4;j++){
				hn=fm25read_data16(h);
				k=fm25read_data8(h+2);
				m=((hn>>8)&0x00ff)^(hn&0x00ff)^0x55;
				if(k==m){
					if(n==0)
						lineX[i][j]=hn;
					else
						lineY[i][j]=hn;
				}
				else
					return(2);// 2= 需定位，資料回初值 
				/*
				else{
					fm25writ_data8(0x0010,0);
					error(9);//定位資料錯誤
				}*/
				h=h+3;
			}
			h&=0xfff0;
			h+=0x10;
		}
	}
	i=fm25read_data8(0x0010); //0X99==OK
  	if(i<2)
  	  	return(1);//ng 1= 需定位，資料不回初值
  	if(i==2)
  	  	return(2);//ng 2= 需定位，資料回初值
	return(0);//ok
	
}
	

void save_XYdata(void)
{
 unsigned char i,j,n;
 unsigned int h,hn;	
  	  
  h=0x0200;
  for(n=0;n<=1;n++){
  	  if(n!=0)
		h=0x0300;
	  for(i=0;i<=2;i++){
			for(j=0;j<=4;j++){
				if(n==0)
					hn=lineX[i][j];
				else
					hn=lineY[i][j];
				
				Ramint_save(h,hn);
				h=h+3;
			}
			h&=0xfff0;
			h+=0x10;
		}
	}

}


void save_lineX_single(unsigned char i,unsigned char j)
{
 unsigned int h,hn;	
 	h=0x0200;
    if(i==1)	  
  		h=0x0210;
  	else if(i==2)	  
  		h=0x0220;
  	else;
  	h+=j*3;	
  	hn=lineX[i][j];
  	Ramint_save(h,hn);

}	
	
void save_lineY_single(unsigned char i,unsigned char j)
{
  unsigned int h,hn;	
 	h=0x0300;
    if(i==1)	  
  		h=0x0310;
  	else if(i==2)	  
  		h=0x0320;
  	else;
  	h+=j*3;	
  	hn=lineY[i][j];
  	Ramint_save(h,hn);	  
  	  
}		
	

void TSnow_rst(void)
{
	TSnowNO=0;
   	fm25writ_data8(0x002d,TSnowNO);
   	TS_out_M=0;////清除已促銷
	fm25writ_data8(0x002c,TS_out_M);
}
	
	

void ram_data_load(void)
{		
 unsigned char a,i,k;
 //unsigned int h;		
 //bit b;	
 	a=coin1_data_out();
 	if(a!=0){
 			coin1_data_init();
 			a=coin1_data_out();
 			if(a!=0)
 					error(8);
 	}		
 	if(coin1_set!=5 && coin1_set!=10)
 		coin1_data_init();
 	if(coin1_play!=5 && (coin1_play%10)!=0)
 		coin1_data_init();
 	
 	
 	
 	playtime_M=fm25read_data8(0x0022);
 	if(playtime_M>60 ||playtime_M<10){
 			playtime_data_init();
 			playtime_M=fm25read_data8(0x0022);
 			if(playtime_M>60 ||playtime_M<10)
 					error(8);
 	}		
	coinplayCount_M=fm25read_data16(0x0066);
 		i=fm25read_data8(0x0068);
	 	a=(coinplayCount_M>>8)&0x00ff;
	 	k=coinplayCount_M&0x00ff;
	 	if(i!=(a^k^0x55)){
	 		 coinplayCount_M=0;
	 		 Ramint_save(0x0066,0);
	 	}	 
	 
	catchcoin_M=fm25read_data8(0x0024); 	
	attmusic_M=fm25read_data8(0x0023);
 	if(attmusic_M>30)
 			attmusic_data_init();
	
	freeplay_M=fm25read_data8(0x0025);
	//freeplay_M=0;
	
	gmoneyDspTime_M=fm25read_data8(0x0044);
	if(gmoneyDspTime_M<3){
		gmoneyDspTime_M=3; //顯示時間
		fm25writ_data8(0x0044,gmoneyDspTime_M);
	}
		
	TSno_M=fm25read_data8(0x002a);
	if(TSno_M>99 ||TSno_M<1){
 			TSno_M=50;
 			fm25writ_data8(0x002a,TSno_M);
 	}	
	win_modM=fm25read_data8(0x002b);
	if(win_modM!=0){
		 if(win_modM!=0x0f){
		 	 win_modM=0;
 			 fm25writ_data8(0x002b,win_modM);
 		 }
 	}
 	TS_out_M=fm25read_data8(0x002c);
 	
	TSnowNO=fm25read_data8(0x002d);
	if(TSnowNO>TSno_M)
		TSnow_rst();
	
	volume_M=fm25read_data8(0x0012);
 	if(volume_M>10 ||volume_M<1){
 			volume_M=5;
 			fm25writ_data8(0x0012,volume_M);
 	}	
 	bo_adjM=fm25read_data8(0x002e);
 	if(bo_adjM>10){
 			bo_adjM=0;
 			fm25writ_data8(0x002e,bo_adjM);
 	}	
 	payoutM_load();
 	if(payout_M>790 || payout_M<10 )
 		payoutM_init();
 	a=payout_M%10;
 	if(a!=0)
 		payoutM_init();
 	i=gmoney_load();
 	if(i!=0){
 			gmoney_data_init();
 	}	
 //	load_xline();
 //	load_yline();
} 	

void coin_save_ram(void)
{
 unsigned char i;	
  	fm25writ_data16(0x0020,credit_int);
  	i=((credit_int>>8)&0x00ff)^(credit_int&0x00ff)^0x55;
	fm25writ_data8(0x0029,i); 
}  		


void credit_00(void)
{
  //unsigned char a;
    credit_int=0;	  
    coin_save_ram();
    play=0;
}  		
void check_credit(void)
{
  unsigned char a,k,i;
  
  i=fm25read_data8(0x0029);
  credit_int=fm25read_data16(0x0020);
  a=(credit_int>>8)&0x00ff;
  k=credit_int&0x00ff;
  if(i!=(a^k^0x55))
      credit_00();
  
  //play=credit_int/playcoin;
}  	


void disp_dec8_set(unsigned char m,unsigned char n)
{		
  unsigned char i;		
					 hex_dec8(m);
					 if(hexdec[1]==0)
    				 		 i=0x20;
    				 else
    				 		 i=0x30+hexdec[1];
					 
    				 spi_data(0,n);//位址n
    				 spi_data(1,i); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
    				 spi_data(0,n);//游標位址
		
}		
void disp_dec83(unsigned char h,unsigned char n)
{
  unsigned char i;		
					 hex_dec8(h);
					 spi_data(0,n);//位址n
    				 spi_data(1,0x20); //
					 if(hexdec[2]==0)
    				 		 i=0x20;
    				 else
    				 		 i=0x30+hexdec[2];
					 
    				 spi_data(1,i); //
 					 if(hexdec[2]==0 && hexdec[1]==0)
 					 		 i=0x20;
    				 else
    				 		 i=0x30+hexdec[1];
    				 spi_data(1,i); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
 					 spi_data(0,(n+1));//游標位址
		
}	

/*
void disp_dec8dot_set(unsigned char m,unsigned char n)
{		
  unsigned char i;		
					 hex_dec8(m);
					 i=0x30+hexdec[1];
    				 spi_data(0,n);//位址n
    				 spi_data(1,i); //
    				 spi_data(1,0x2e); //.
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
    				 spi_data(0,n);//游標位址
		
}		
void disp_int_set(unsigned int h,unsigned char n)
{
  unsigned char i;		
					 hex_dec(h);
					 spi_data(0,n);//位址n
    				 spi_data(1,0x20); //
					 if(hexdec[2]==0)
    				 		 i=0x20;
    				 else
    				 		 i=0x30+hexdec[2];
					 
    				 spi_data(1,i); //
 					 if(hexdec[2]==0 && hexdec[1]==0)
 					 		 i=0x20;
    				 else
    				 		 i=0x30+hexdec[1];
    				 spi_data(1,i); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
 					 spi_data(0,(n+1));//游標位址
		
}*/		

void disp_int3_set(unsigned int h,unsigned char n)
{
  unsigned char i;
  bit b;		
					 hex_dec(h);
					 spi_data(0,n);//位址n
					 b=0;
    				 if(hexdec[3]==0 && hexdec[4]==0)
    				 		 i=0x20;
    				 else{
    				 		 i=0x30+hexdec[3];
    				 		 b=1;
					}
    				 spi_data(1,i); //
    				 
					 if(b==0 && hexdec[2]==0)
    				 		 i=0x20;
    				 else{
    				 		 i=0x30+hexdec[2];
    				 		 b=1;
					}
    				 spi_data(1,i); //
 					 if(b==0 && hexdec[1]==0)
 					 		 i=0x20;
    				 else{
    				 		 i=0x30+hexdec[1];
    				 		 b=1;
    				 }
    				 spi_data(1,i); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
 					 //spi_data(0,(n+1));//游標位址
		
}						
void disp_int4_set(unsigned int h,unsigned char n)
{
  unsigned char i;
  bit b;		
					 hex_dec(h);
					 spi_data(0,n);//位址n
					 if(hexdec[4]==0){
    				 		 i=0x20;
    				 		 b=0;
    				 }		 
    				 else{
    				 		 i=0x30+hexdec[4];
    				 		 b=1;
					 }
    				 spi_data(1,i); //
 					 if(b==0 && hexdec[3]==0)
 					 		 i=0x20;
    				 else{
    				 		 i=0x30+hexdec[3];
    				 		 b=1;
    				 }		 
    				 spi_data(1,i); //
					 if(b==0 && hexdec[2]==0)
    				 		 i=0x20;
    				 else{
    				 		 i=0x30+hexdec[2];
    				 		 b=1;
    				 }		 
    				 spi_data(1,i); //
 					 if(b==0 && hexdec[1]==0)
 					 		 i=0x20;
    				 else
    				 		 i=0x30+hexdec[1];
    				 spi_data(1,i); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
 					// spi_data(0,(n+1));//游標位址
		
}	
//顯示4個字，後面都插入1個空白
void disp_int_lcm_set(unsigned int h,unsigned char n)
{
  unsigned char i;		
					 hex_dec(h);
					 spi_data(0,n);//位址n
    				 i=0x30+hexdec[3];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
    				 i=0x30+hexdec[2];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
    				 i=0x30+hexdec[1];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
		
}	
//int 全顯示，如前面為0，則不顯示
void disp_int_lcm_all(unsigned int h,unsigned char n)
{
  unsigned char i;
  bit b=0;		
					 hex_dec(h);
					 spi_data(0,n);//位址n
					 if(hexdec[4]==0)
    				 		 i=0x20;
    				 else{
    				 	 i=0x30+hexdec[4];
    				 	 b=1;
    				 }	 
    				 spi_data(1,i); //
    				 if(b==0 && hexdec[3]==0)
    				 		 i=0x20;
    				 else{
    				 	 i=0x30+hexdec[3];
    				 	 b=1;
    				 }	 
    				 spi_data(1,i); //
    				 if(b==0 && hexdec[2]==0)
    				 		 i=0x20;
    				 else{
    				 	 i=0x30+hexdec[2];
    				 	 b=1;
    				 }	 
    				 spi_data(1,i); //
    				 if(b==0 && hexdec[1]==0)
    				 		 i=0x20;
    				 else{
    				 	 i=0x30+hexdec[1];
    				 	 b=1;
    				 }	 
    				 spi_data(1,i); //
    				 i=0x30+hexdec[0];
    				 spi_data(1,i); //
    				 spi_data(1,0x20); //
		
}			
/*			
void check_IN8000_off(unsigned char a)		
{
  unsigned char i;
 		do{
 			  delay1ms(30);	
 			  i=IN8000;
 		}while((i&a)==0);
		
}*/		 		
void check_INA000_off(unsigned char a)		
{
  unsigned char i;
 		do{
 			  delay1ms(30);	
 			  i=INA000;
 		}while((i&a)==0);
		
}
void check_INC000_off(unsigned char a)		
{
  unsigned char i;
 		do{
 			  delay1ms(30);	
 			  i=INC000;
 		}while((i&a)==0);
		
}	
void check_INE000_off(unsigned char a)		
{
  unsigned char i;
 		do{
 			  delay1ms(30);	
 			  i=INE000;
 		}while((i&a)==0);
		
}	
/*
void disp_Woff(unsigned char a)
{
	spi_data(0,a);
    spi_data(1,0x4f);//o
 	spi_data(1,0x46);//f
 	spi_data(1,0x46);//f
    spi_data(1,0x20);// 
    spi_data(0,a);
}	
void disp_Won(unsigned char a)
{
	spi_data(0,a);
    spi_data(1,0x4f);//o
 	spi_data(1,0x4e);//n
 	spi_data(1,0x20);// 
 	spi_data(1,0x20);// 
 	spi_data(0,a);
}	
*/
void disp_gmoney_M(unsigned char a) //顯示歸零時機
{
	unsigned char i;
	i=fm25read_data8(0x0007);
	switch(i){
				case 1:W_DISP(a,WSC11); //設定        歸零
    				 		break;
    			case 2:W_DISP(a,WSC12); //中獎        歸零
    				 		break; 
    			case 3:W_DISP(a,WSC13); //不正常關機  歸零
    				 		break;
    			default:W_DISP(a,WSC10); //初始化      歸零
    				 		break; 
    }
}
		
//設定功能
void  setting(void)
{
unsigned char i,j,k,ma,m,n,index;
 unsigned char MK0,MR0,MK1,MR1;
 //signed int hh;
 unsigned int h;
 bit b,b2,AB;		
 
 
    voice_dodo(2);
    check_INC000_off(0x02);
    LCM_clr();
    spi_data(0,0x30);//function set RE=0
    delayus(50);
    
    ma=1;
    index=2; //第2行
    MK0=1;
    MR0=1;
   //ot_meter|=0x20; //中獎燈 on 
 while(1){   
    	b=0;
    	AB=0;
    	while(ma==1){
    		if(AB==0){
  	   		   AB=1;
  	   		   k=MK0;
    		   m=MR0;
    		   index=(k-m)+2; //第? 行
    		}		
     		if(b==0){		
     			W_DISP(0x80,WFSET);
     			b=1;
     			switch(m){ //menu
    		 		case 1:
    		 			W_DISP(0x90,WSA);
    		    		W_DISP(0x88,WSB);
    		 			W_DISP(0x98,WSC);
    					break;
    				case 2:
    		 			W_DISP(0x90,WSB);
    		    		W_DISP(0x88,WSC);
    		 			W_DISP(0x98,WSD);
    					break;
    			    case 3:
    		 			W_DISP(0x90,WSC);
    		    		W_DISP(0x88,WSD);
    		 			W_DISP(0x98,WSE);
    					break;		
 		 		}
 		 		i=0x80;
    	 		if(index==1)
    	 	 		i=0x80; //第一行
    	 		else if(index==2)
    	 	 		i=0x90; //第2行
    	 		else if(index==3)
    	 	 		i=0x88; //第3行	 
    	 		else if(index==4)
    	 	 		i=0x98; //第4行	 
    	 		else;	 
    	 		spi_data(0,0x0F); //display on/ cursor oN
    	 		spi_data(0,i);//位址X
    	 		tep_t10=15000; //30 sec
    	 
      		}
      		delay1ms(30);
      		i=INA000;
 	  		if((i&0x08)==0){ //搖桿往右 
 				 check_INA000_off(0x08);
 				 	ma=10+k;
 				 	MR0=m;
 				 	MK0=k;
 				 	MK1=1;
    		   		MR1=1;
 				 	break;
 	  		}
 	  		else if((i&0x01)==0){ //搖桿往前     	
    			check_INA000_off(0x01);
    			if(k>1){
 			        k--;
 			        if(index>2)
 			        	index--;
 			        else if(index==2){
 			        		if(m>1)
 			        			m--;
 			        }
 			        else;
 			        b=0;		
 			     }
 			     continue;			
      		}
      		else if((i&0x02)==0){ //搖桿往後
    			check_INA000_off(0x02);
    			if(k<5){
 			         k++;
 			        if(index<4)
 			        	index++;
 			        else if(index==4){
 			        		if(m<3)
 			        			m++;
 			        }
 			        else;
 			       b=0; 			
 			     }
 			     continue;
    		}
    		else if((i&0x04)==0 || tep_t10==0){ //搖桿往左 回上一層
    			check_INA000_off(0x04);
    			ma=255;
    			break;
    		}
    	
    		else;
    	}	//while(ma==1)
    	if(ma==255)
    		break;	
    b=0;
    AB=0;
  	while(ma==11){ //setting
  		if(AB==0){
  	   		   AB=1;
  	   		   k=MK1;
    		   m=MR1;
    		   index=(k-m)+2; //第? 行
    	}	
     	if(b==0){		
     			W_dspace(0x80);
     			W_DISPh(0x81,2,15,WSA);	
     			b=1;
 				switch(m){
    		 			case 1:
    		 				W_DISP(0x90,WSA1);
    		    			W_DISP(0x88,WSA2);
    		 				W_DISP(0x98,WSA3);
    						break;
    		 			case 2:
    		    			W_DISP(0x90,WSA2);
    		    			W_DISP(0x88,WSA3);
    		 				W_DISP(0x98,WSA4);
    						break;
    					case 3:
    		    			W_DISP(0x90,WSA3);
    		    			W_DISP(0x88,WSA4);
    		 				W_DISP(0x98,WSA5);
    						break;	
    					case 4:
    		    			W_DISP(0x90,WSA4);
    		    			W_DISP(0x88,WSA5);
    		 				W_DISP(0x98,WSA6);
    						break;
    					case 5:
    		    			W_DISP(0x90,WSA5);
    		    			W_DISP(0x88,WSA6);
    		 				W_DISP(0x98,WSA7);
    						break;
    				   	case 6:
    		    			W_DISP(0x90,WSA6);
    		    			W_DISP(0x88,WSA7);
    		 				W_DISP(0x98,WSA8);
    						break;	
    				   		
    						
    	 		} 	
 		 	    i=0x80;
    	 		if(index==1)
    	 	 		i=0x80; //第一行
    	 		else if(index==2)
    	 	 		i=0x90; //第2行
    	 		else if(index==3)
    	 	 		i=0x88; //第3行	 
    	 		else if(index==4)
    	 	 		i=0x98; //第4行	 
    	 		else;	 
    	 		spi_data(0,0x0F); //display on/ cursor oN
    	 		spi_data(0,i);//位址X
    	 		tep_t10=15000; //30 sec
    	 		
 		}
      	delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ //搖桿往右 
 				 check_INA000_off(0x08);
 				 	ma=20+k;
 				 	MR1=m;
 				 	MK1=k;
 				 	break;
 	  	}
 	  	else if((i&0x01)==0){ //搖桿往前     	
    			check_INA000_off(0x01);
    			if(k>1){
 			        k--;
 			        if(index>2)
 			        	index--;
 			        else if(index==2){
 			        		if(m>1)
 			        			m--;
 			        }
 			        else;	
 			        b=0;	
 			     }
 			     
 			     continue;			
      }
      else if((i&0x02)==0){ //搖桿往後
    			check_INA000_off(0x02);
    			if(k<8){
 			         k++;
 			        if(index<4)
 			        	index++;
 			        else if(index==4){
 			        		if(m<6)
 			        			m++;
 			        }
 			        else;
 			      b=0;  			
 			     }
 			     continue;
    	}
 	  else if((i&0x04)==0 || tep_t10==0){ //搖桿往左 回上一層
    				check_INA000_off(0x04);
    				ma=1;
    				if(tep_t10==0)
    					ma=255;
    				break;
    			}
    			else;
    }//while(ma==11)	
    if(ma==1)
    		continue;
    b=0;
    b2=0;
    AB=0;
  	while(ma==12){ //payout setting
     	if(b==0){		
     			W_dspace(0x80);
     			W_DISPh(0x81,2,15,WSB);		 
     			b=1;
     			W_DISP(0x90,WA119);
     			W_DISP(0x98,Wspace);
     			W_DISP(0x88,WSA42);
    			h=payout_M;
    				 disp_int_lcm_set(h,0x89);
    				 
    				 j=1;
    				 i=0x8a;
    				 spi_data(0,i);//位址X
    				 tep_t10=15000; //30 sec
    	  	      }	//if(b==0)				
    			  delay1ms(30);	
 				  i=INA000;
    			if((i&0x01)==0){ //搖桿往前
 				  		hex_dec(h);
 				  		if(j==0){
 				  				i=0x89; //第一行
 				  				//if(hexdec[3]<9)
 				  					//	h+=1000;
 				  		}
    	 				else if(j==1){
    	 						i=0x8a; //第2行
    	 						if(hexdec[2]<7)
 				  						h+=100;
 				  		}
    	 				else if(j==2){
    	 						i=0x8b; //第3行	 
    	 						if(hexdec[1]<9)
 				  						h+=10;
 				  		}
    	 				else if(j==3){
    	 						i=0x8c; //第4行	 
    	 						//if(hexdec[0]<9)
 				  						//h+=1;
 				  		}
    	 				else;	 
    	 				if(h>790)
    	 						h=790;
 				  		disp_int_lcm_set(h,0x89);
 				  		spi_data(0,i);//位址X
 				  		check_INA000_off(0x01);
 				  		tep_t10=15000; //30 sec
        		 }		
        		 else if((i&0x02)==0){ //搖桿往後
 				  		hex_dec(h);
 				  		if(j==0){
 				  				i=0x89; //第一行
 				  				//if(hexdec[3]>0)
 				  					//	h-=1000;
 				  		}
    	 				else if(j==1){
    	 						i=0x8a; //第2行
    	 						if(hexdec[2]>0)
 				  						h-=100;
 				  		}
    	 				else if(j==2){
    	 						i=0x8b; //第3行	 
    	 						if(hexdec[1]>0)
 				  						h-=10;
 				  		}
    	 				else if(j==3){
    	 						i=0x8c; //第4行	 
    	 						/*
    	 						if(h>1){
    	 							if(hexdec[0]>0)
 				  						h-=1;
 				  				}	*/
 				  		}
    	 				else;	 
    	 				if(h<10)
    	 						h=10;
 				  		disp_int_lcm_set(h,0x89);
    	 				spi_data(0,i);//位址X
 				  		check_INA000_off(0x02);
 				  		tep_t10=15000; //30 sec
        		 }		
        		 else if((i&0x08)==0){ //搖桿往右 
 				  	if(j<2)
 				  		j++;
 				  	i=0x89;
    	 			if(j==0)
    	 	 			i=0x89; //第一行
    	 			else if(j==1)
    	 	 			i=0x8a; //第2行
    	 			else if(j==2)
    	 	 			i=0x8b; //第3行	 
    	 			else if(j==3)
    	 	 			i=0x8c; //第4行	 
    	 			else;	 
    	 			spi_data(0,i);//位址X
 				 	check_INA000_off(0x08);
 				 	tep_t10=15000; //30 sec
        		 }		
        		 else if((i&0x04)==0){ //搖桿往左 
 				  	if(j>1)
 				  		j--;
 				  	i=0x89;
    	 			if(j==0)
    	 	 			i=0x89; //第一行
    	 			else if(j==1)
    	 	 			i=0x8a; //第2行
    	 			else if(j==2)
    	 	 			i=0x8b; //第3行	 
    	 			else if(j==3)
    	 	 			i=0x8c; //第4行	 
    	 			else;	 
    	 			spi_data(0,i);//位址X
 				 	check_INA000_off(0x04);
 				 	tep_t10=15000; //30 sec
        		 }		
        		 else if((i&0x20)==0 || tep_t10==0){ //1
 				  	check_INA000_off(0x20);
 				  	if(h!=payout_M){
 				  		payout_M=h; //
        		  	  	Ramint_save(0x0060,payout_M); 
        		  	  	game_bankM_0();
 						gmoney_00();////累加金額歸零
 						fm25writ_data8(0x0007,0x01);//01=設定
        		  	  	credit_00();
        		  	    save_data(); //modify
 				  		
 				  	}
 				    ma=1;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
 			    }   
                else;		 
    }//while(ma==12)	
    if(ma==1)
    		continue; 	
    b=0;
  	while(ma==21  ){ //coin vs play
     	if(b==0){	 
  			 		 b=1;
  			 		 W_dspace(0x80);
  			 		 W_DISPh(0x81,2,15,WSA1);
  			 		 W_DISP(0x90,WA111);
  			 		 W_DISP(0x88,WSA10);
  			 		 W_DISP(0x98,WSA11);
  			 		 m=coin1_set;
    				 n=coin1_play;
 					 disp_dec8_set(n,(0x98+3));//
 					 disp_dec8_set(m,(0x88+3));//
 					 tep_t10=15000; //30 sec
 					 k=1;
    	  	}	//if(b==0)				
    		delay1ms(30);
    		i=INA000;
 			if((i&0x08)==0){ //搖桿往右 
 				if(k==1){
 					k=2;
 					spi_data(0,0x98+3);
 				}
 				check_INA000_off(0x08);	
 				tep_t10=15000; //30 sec
 			}     
 			else if((i&0x04)==0){ //搖桿往左 
    				if(k==2){
        		 		 k=1;
        		 		 spi_data(0,0x88+3);//位址X
        		 	}
    				check_INA000_off(0x04);
    				tep_t10=15000; //30 sec
    		}
 			else if((i&0x01)==0){ //搖桿往前
 				if(k==1){
 					if(m==5){
 						m=10;
 						disp_dec8_set(m,(0x88+3));//
 					}
 				}
 				else{
 					if(n<10)
 						n=10;
 				  	else if(n<90)
 				  		n+=10;
 				  	disp_dec8_set(n,(0x98+3));//
 				 }	
 			      check_INA000_off(0x01);
 			      tep_t10=15000; //30 sec
 			}      	
 			else if((i&0x02)==0){ //搖桿往後
 					if(k==1){
 				  			if(m==10){
 				  			  	m=5;
 				  				disp_dec8_set(m,(0x88+3));//
 				  			}
 				  	}
 				  	else{
 				  		if(n<=10)
 							n=5;
 				  		else if(n>=20)
 				  				n-=10;
 				  		disp_dec8_set(n,(0x98+3));//
 				  	}		
 			        check_INA000_off(0x02);
 			        tep_t10=15000; //30 sec
 			        	
 			} 
    		else if((i&0x20)==0 || tep_t10==0){ //catch
 				  	check_INA000_off(0x20);
 				  	coin1_set=m;
    				coin1_play=n;
 				  	coinplay1_set_save();
 				  			
 				  	save_data(); //exit
 				    ma=11;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
    		}
    		else;
    }//while(ma==11)	
    if(ma==11)
    		continue;   	
     b=0;
  	while(ma==22  ){ //中獎模式
     		if(b==0){	 
  			 		 b=1;
  			 		 W_dspace(0x80);
  			 		 W_DISPh(0x81,2,15,WSA2);
  			 		 W_DISP(0x98,Wspace);
  			 		 m=TSno_M;//促銷次數
  			 		 n=win_modM;
  			 		 b2=0;
  			 		 k=1;
  			}
  			if(b2==0){	 
  			 		 b2=1;
  			 		 if(win_modM==0){ //固定
  			 		 		W_DISP(0x90,WSA221);
  			 		 		W_DISP(0x88,Wspace);
  			 		 		W_DISP(0x98,Wspace);
  			 		 		
  			 		 }
  			 		 else{
  			 		 	 W_DISP(0x90,WSA222); //促銷
  			 		 	 W_DISP(0x88,WSA223);
  			 		 	 disp_dec83(m,0x88+2); //亂數    次內 1次
  			 		 	 if(TS_out_M==0) //未促銷
  			 		 	 	W_DISP(0x98,WSA224); //已累積  次; 未出
  			 		 	 else
  			 		 	 	W_DISP(0x98,WSA224A); //已累積  次; 已出 	
  			 		 	 disp_dec8_set(TSnowNO,0x98+3);//
  			 		 	 
  			 		 	 	 
  			 		 }
 					 tep_t10=15000; //30 sec
 					 spi_data(0,0x90+4);
 					 check_INA000_off(0x01);
 					 check_INA000_off(0x02);
    	  	}	//if(b==0)				
    		delay1ms(30);
    		i=INA000;
 			if((i&0x08)==0){ //搖桿往右 
 				if(win_modM!=0){
 					if(k==1){
 						k=2;
 						spi_data(0,0x88+3);
 					}
 				}
 				check_INA000_off(0x08);	
 				tep_t10=15000; //30 sec
 			}     
 			else if((i&0x04)==0){ //搖桿往左 
    				if(k==2){
        		 		 k=1;
        		 		 spi_data(0,0x90+4);//位址X
        		 	}
    				check_INA000_off(0x04);
    				tep_t10=15000; //30 sec
    		}
 			else if((i&0x01)==0){ //搖桿往前
 				if(k==1){
 					if(win_modM==0) //固定
 						win_modM=0x0f;
 					else
 						win_modM=0x00;	
 					b2=0;
 					continue;
 				}
 				else{
 					if(m>=99)
 						m=1;
 					else
 						m++;
 					disp_dec83(m,0x88+2);//
 					delay100ms(3);
 					tep_t10=15000; //30 sec
 				}
 			}
 			else if((i&0x02)==0){ //搖桿往後
 				if(k==1){
 					if(win_modM==0) //固定
 						win_modM=0x0f;
 					else
 						win_modM=0x00;	
 					b2=0;
 					continue;
 				}
 				else{
 					if(m<=1)
 						m=99;
 					else
 						m--;
 					disp_dec83(m,0x88+2);//
 					delay100ms(3);
 					tep_t10=15000; //30 sec
 				}
 			}	
 			else if((i&0x20)==0 || tep_t10==0){ //catch
 				  	check_INA000_off(0x20);
 				  	if(TSno_M!=m)
 						TSnow_rst();
 					if(win_modM!=n)
 						TSnow_rst();
 				  	TSno_M=m;//促銷次數
 				  	fm25writ_data8(0x002a,TSno_M);
 					fm25writ_data8(0x002b,win_modM);
 							
 				  	save_data(); //exit
 				    ma=11;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
    		}
    		else;	
    }	
 	if(ma==11)
    		continue;  		
    
     b=0;
     b2=0;
  	 while(ma==23){ //選物累加功能
     	if(b==0){		
     			b=1;
     			W_dspace(0x80);
     			W_DISPh(0x81,2,15,WSA3);
    			W_DISP(0x90,WSA31);
  			 	W_DISP(0x88,WSA32);
  			 	n=gmoneyDspTime_M;	//(顯示保留時間)
  		}
  		if(b2==0){
  				b2=1;
  			 	if(n==0xff)
  			 			W_DISP(0x98,WSA33A);
  			 	else{
  			 		W_DISP(0x98,WSA33);
  			 		disp_dec8_set(n,0x98+5);
  			 	}
  			 	spi_data(0,0x98+5);
  			 	check_INA000_off(0x01);
     			check_INA000_off(0x02);
    		  	tep_t10=15000; //30 sec
  			 	
  		}
  		delay1ms(30);
    	i=INA000;
    	if((i&0x01)==0){ //搖桿往前
    			if(n==0xff){
 					n=3;
 					b2=0;
 					continue;
 				}
 				else if(n>=30){
 					n=0xff;
 					b2=0;
 					continue;
 				}
 				else{
 					n++;
 					b2=0;
 					continue;
 				}
 		}
  		else if((i&0x02)==0){ //搖桿往後
  				if(n>30){
 					n=30;
 					b2=0;
 					continue;
 				}
 				else if(n==3){
 					n=0xff;
 					b2=0;
 					continue;
 				}
 				else{
 					n--;
 					b2=0;
 					continue;
 				}
 		}
  		else if((i&0x20)==0 || tep_t10==0){ //catch
 				  	check_INA000_off(0x20);
 				  	gmoneyDspTime_M=n;
 				  	fm25writ_data8(0x0044,gmoneyDspTime_M);
 					save_data();
 				    ma=11;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
 			    }   
                else;		 
   }	//while(ma==24 )
    if(ma==11)
    		continue;  	
    
     b=0;
  	while(ma==24 || ma==27 || ma==28){ //play time
  				
     			if(b==0){		
     					W_dspace(0x80);
     					if(ma==24){
     						W_DISPh(0x81,2,15,WSA4);
     						W_DISP(0x98,WSA42A);	
     						m=playtime_M;
     					}
     					else if(ma==27){
     						W_DISPh(0x81,2,15,WSA7);
     						W_DISP(0x98,WSA42);	
     						m=bo_adjM;
     					}
     					else{
     						W_DISPh(0x81,2,15,WSA8);
     						W_DISP(0x98,WSA42);	
     						m=volume_M;
     						bank_6295(0);
     						ic_6295_ch(0x08,3,0);	//play music
     					}
     						
  			 		 	W_DISP(0x90,WB12);
  			 		 	W_DISP(0x88,Wspace);
     					 
     					b=1;
 						
     					disp_dec8_set(m,0x99);
    		  		   	tep_t10=15000; //30 sec
    			} //if(b==0)
    			delay1ms(30);
    			i=INA000;
    			if((i&0x01)==0){ //搖桿往前
    				if(ma==24){
 				  			if(m>=60)
 				  				m=60;
 				  			else
 				  			 m++;
 				  	}
 				  	else if(ma==27){
 				  		if(m>=10)
 				  				m=10;
 				  		 else
 				  			 m++;
 				  	}
 				  	else{
 				  		if(m>=10)
 				  				m=10;
 				  		else
 				  			 m++;
 				  		volume_M=m;
     					ic_6295_ch(0x08,3,0);	//play music
 				  	}
 				  		disp_dec8_set(m,0x99);
           		  		delay100ms(3);	
           		  		tep_t10=15000; //30 sec
        		 }		
        		 else if((i&0x02)==0){ //搖桿往後
        		 	 	if(ma==24){
 				  			if(m>10)
 				  			  m--;
 				  		}
 				  		else if(ma==27){
 				  			if(m>0)
 				  			  m--;
 				  		}
 				  		else{	
 				  			if(m>1)
 				  			  m--;
 				  			volume_M=m;
     						ic_6295_ch(0x08,3,0);	//play music
 				  		}
 				  		disp_dec8_set(m,0x99);
 				  		delay100ms(3);
 				  		tep_t10=15000; //30 sec
        		 }		
        		 else if((i&0x20)==0 || tep_t10==0){ //catch
 				  	check_INA000_off(0x20);
 				  	if(ma==24){	
 				  		playtime_M=m;
 						fm25writ_data8(0x0022,playtime_M);
 					}
 					else if(ma==27){	
 				  		bo_adjM=m;
 						fm25writ_data8(0x002e,bo_adjM);
 					}
 					else{
 						volume_M=m;
 						fm25writ_data8(0x0012,volume_M);
 					}
 					save_data();
 				    ma=11;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
 			    }   
                else;		 
   }	//while(ma==24 )
    if(ma==11)
    		continue;  	
     
    b=0;
    b2=0;
    AB=0;
  	while(ma==25){ //Attract Music
  				if(b==0){		
     					W_dspace(0x80);
     					W_DISPh(0x81,2,15,WSA5);
  			 		 	W_DISP(0x90,WB12);
     					W_DISP(0x98,Wspace);
     					b=1;
     					k=1;
     					m=attmusic_M;
     			 }
     			 if(b2==0){		
     			 		 b2=1;
     			 		 if(m==0){
     							W_DISP(0x88,WSA42);
     							spi_data(0,0x8b);
     							spi_data(1,0x4f);//o
 								spi_data(1,0x46);//f
 								spi_data(1,0x46);//f
     							spi_data(1,0x20);// 
     					}
     					else{
     							W_DISP(0x88,WSA5A);
     							disp_dec8_set(m,0x8b);
     					}	
     					 
     					check_INA000_off(0x01);
     					check_INA000_off(0x02);
    		  		   	tep_t10=15000; //30 sec
    			} //if(b2==0)
     			
    			delay1ms(30);
    			i=INA000;
    			if((i&0x01)==0){ //搖桿往前
    				if(k==1){
 				  				if(m<30)
 				  					m++;		
 				  				b2=0;
 				  	 }
 				  	continue;
    			 }			
        		 else if((i&0x02)==0){ //搖桿往後
        		 	if(k==1){
        		 		if(m!=0)
 				  			 m--;
 				  		b2=0;
 				  	}
 				  	continue;	
        		 }	
        		 else if((i&0x04)==0){ //搖桿往左 
    			 	check_INA000_off(0x04);
    			}
        		 else if((i&0x08)==0){ //搖桿往右 
        		    check_INA000_off(0x08);
        		}
        		 else if((i&0x20)==0 || tep_t10==0){ //catch
 				  	check_INA000_off(0x20);
 				  	attmusic_M=m;
 					fm25writ_data8(0x0023,attmusic_M);
 					save_data();
 				    ma=11;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
 			    }   
                else;		 
   }	//while(ma==23 )
    if(ma==11)
    		continue;  
    b=0;
    b2=0;
  	while(ma==26  ){ //coin save
     			if(b==0){		
     					b=1;
     					W_dspace(0x80);
     					W_DISPh(0x81,2,15,WSA6);
     					W_DISP(0x90,WSA61);
     					W_DISP(0x88,Wspace);
     					W_DISP(0x98,WSA62);
    					spi_data(0,0x98);
     					tep_t10=15000; //30 sec
    			} //if(b==0)
    			delay1ms(30);
    			m=0;
    			i=INA000;
        		if((i&0x20)==0 || tep_t10==0){ //catch
        			do{
        		  	  delay1ms(30);
        		  	  m++;
        		  	  if(m>=65){
        		  	  	  	game_bankM_0();
 							gmoney_00();////累加金額歸零
 							fm25writ_data8(0x0007,0x01);//01=設定
        		  	  		credit_00();
        		  	  		spi_data(0,0x90+3);
        		  	  		spi_data(1,0xc2);//
 							spi_data(1,0x6b);//歸
 							spi_data(1,0xb9);//
 							spi_data(1,0x73);//零
        		  	  		voice_dodo(3);
        		  	  		W_DISP(0x90,WSA61);  //目前=儲存
        		  	  		delay100ms(10);
        		  	  }	  
        		      i=INA000;
        		  }while((i&0x20)==0);  	  
        		  check_INA000_off(0x20);	
        		  ma=11;
 				    break;
 			 }	  	  
        	 else if((i&0x04)==0 || tep_t10==0){ //搖桿往左 回上一層
    			check_INA000_off(0x04);
    				ma=11;
    				if(tep_t10==0)
    					ma=255;	
    				break;
    		}
    		else; 	  	  	  
   }	//while(ma==26)
   
    if(ma==11)
    		continue;  	 
    
    b=0;
    AB=0;
    b2=0;
  	while(ma==13){ //Bookkeeping
  		if(AB==0){
  	   		   AB=1;
  	   		   k=MK1;
    		   m=MR1;
    		   index=(k-m)+2; //第? 行
    	}
     	if(b==0){		
     			W_dspace(0x80);
     			W_DISPh(0x81,2,15,WSC);	
     			b=1;
 				W_DISP(0x90,WSC1);
    		    W_DISP(0x88,WSC2);
    		 	W_DISP(0x98,WSC3);
    	}
    	if(b2==0){
    		b2=1;
 		 	    i=0x80;
    	 		if(index==1)
    	 	 		i=0x80; //第一行
    	 		else if(index==2)
    	 	 		i=0x90; //第2行
    	 		else if(index==3)
    	 	 		i=0x88; //第3行	 
    	 		else if(index==4)
    	 	 		i=0x98; //第4行	 
    	 		else;	 
    	 		spi_data(0,0x0F); //display on/ cursor oN
    	 		spi_data(0,i);//位址X
    	 		tep_t10=15000; //30 sec
 		}
 		delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ // 右
 				 check_INA000_off(0x08);
 				 	ma=30+k;
 				 	MR1=m;
 				 	MK1=k;
 				 	//MK2=1;
    		   		//MR2=1;
 				 	break;
 	  	}
 	  	else if((i&0x01)==0){ // 前    	
    			check_INA000_off(0x01);
    			if(k>1){
 			        k--;
 			        if(index>2)
 			        	index--;
 			        b2=0;	
 			     }
 			     
 			     continue;			
      	}
      	else if((i&0x02)==0){ // 後
    			check_INA000_off(0x02);
    			if(k<3){
 			         k++;
 			        if(index<4)
 			        	index++;
 			      	b2=0; 
 			     }
 			     continue;
    	}
 	  	else if((i&0x04)==0 || tep_t10==0){ //1 回上一層
    				check_INA000_off(0x04);
    				ma=1;
    				if(tep_t10==0)
    					ma=255;
    				break;
    	}
    	else;
    }//while(ma==13)	
    if(ma==1)
    		continue; 	
    
    
  	while(ma==14){ //free play
  			if(b==0){		
  				b=1;	
     			W_dspace(0x80);
     			W_DISPh(0x81,2,15,WSD);	
    		 	W_DISP(0x90,WA110);
    		    W_DISP(0x88,Wspace); //空白
    		 	m=freeplay_M;
    		}
    		if(b2==0){	 	
    			b2=1;
    			if(freeplay_M==1) //免費遊戲
 					W_DISP(0x98,WSD2);	
 				else if(freeplay_M==2) //機台測試
 					W_DISP(0x98,WSD3);	
 				else	//正常營業
 					W_DISP(0x98,WSD1);	
    	 		spi_data(0,0x98);//位址X
    	 		tep_t10=15000; //30 sec
      		}
  	
    			delay1ms(30);
    			i=INA000;
    			if((i&0x01)==0){ //搖桿往前
    			   check_INA000_off(0x01);
    			   if(freeplay_M==0)
    			   		   freeplay_M=1;
    			   else if(freeplay_M==1)
    			   		   freeplay_M=0;
    			   else freeplay_M=0;	   
    			   b2=0;
 			       continue;
 				}      	
 				else if((i&0x02)==0){ //搖桿往後
 			        check_INA000_off(0x02);
    			    if(freeplay_M==0)
    			   		   freeplay_M=1;
    			    else if(freeplay_M==1)
    			   		   freeplay_M=0;
    			   else freeplay_M=0;	   
    			   b2=0;
 			       continue;
    			}   
    			else if((i&0x20)==0 || tep_t10==0 ){ //catch
 				  		check_INA000_off(0x20);	
 				  		fm25writ_data8(0x0025,freeplay_M);
 				  		if(m!=freeplay_M){
 				  			game_bankM_0();
 							gmoney_00();////累加金額歸零
 							fm25writ_data8(0x0007,0x01);//01=設定
        		  	  		credit_00();
 				  			save_data();
 				  		}	
 				  		ma=1;
 			      		break;
 				}        
    			else;   
    			   
    }//while(ma==14)	
    if(ma==1)
    		continue;  	
    
    b=0;
  	while(ma==31 || ma==32 || ma==33){ //
  			 	if(b==0){	 
  			 		b=1;	
  			 		k=ma-30;
  			 		W_DISP(0x90,WC20); //o長按歸零;o出
  			 		W_DISP(0x88,Wspace); //空白
  			 		W_DISP(0x98,Wspace); //空白
  			 		W_dspace(0x80);
  			 		if(k==1){
  			 			W_DISPh(0x81,2,15,WSC1);	
  			 			disp_gmoney_M(0x98); //顯示歸零時機
  			 		}
  			 		else if(k==2)
  			 			W_DISPh(0x81,2,15,WSC2);	
  			 		else{ 
  			 			W_DISPh(0x81,2,15,WSC3);
  			 			W_DISP(0x98,WSC31); //不正常關機    次
  			 			i=data8_ram_read(0x0008); //不正常關機次數
  			 			disp_dec83(i,0x98+5);
  			 			
  			 		}
  			 		 spi_data(0,0x88);
  			 		 spi_data(1,0x20);//
 					 spi_data(1,0x3d);//=
 					 if(k==1){	
 							h=gmoney_M;
 							spi_data(1,0x20);//
 					 		spi_data(1,0x20);// 
 					  }
 					   else if(k==2){
 								h=coin_play_M;	
   	   	   						spi_data(1,0x20);//
 					 			spi_data(1,0x20);// 
   	   	   				}	
 						else if(k==3){
 								h=gift_out_M;	
   	   	   						spi_data(1,0x20);//
 					 			spi_data(1,0x20);//
   	   	   				}
   	   	   				else;
    				 	disp_int4_set(h,0x8a);
    				 	spi_data(0,0x8d);
    				 	tep_t10=15000; //30 sec
    			} //if(b==0)
    			  delay1ms(30);	
 				  i=INA000;
 				  m=0;
        		 if((i&0x20)==0 ){ //catch
        		 	do{
        		  	  delay1ms(30);
        		  	  m++;
        		  	  if(m>=70){
        		  	  	  if(k==1){
 							  		game_bankM_0();
 									gmoney_00();////累加金額歸零
 									fm25writ_data8(0x0007,0x01);//01=設定
        		  	  				credit_00();
        		  	  				disp_gmoney_M(0x98); //顯示歸零時機
 						  }
        		  	  	  else if(k==2)
        		  	  		  	  coin_playM_0();
        		  	  	  else{
        		  	  		 gift_outM_0();
        		  	  		 data8_ram_save(0x0008,0);
        		  	  		 i=data8_ram_read(0x0008); //不正常關機次數
  			 				 disp_dec83(i,0x98+5);
  			 			   }
        		  	  		 
        		  	  	 voice_dodo(3);
        		  	  	 b=0;
        		  	  	 break;
        		  	  }	  
        		      i=INA000;
        		   }while((i&0x20)==0);  	  
        		 	check_INA000_off(0x20);	
        		 }		
        		 else if((i&0x04)==0 || tep_t10==0){ ////搖桿往左 
 				  	check_INA000_off(0x04);	
 				    ma=13;
 				    if(tep_t10==0)
    						ma=255;
 				    break;
 			    }   
                else;		 
   }	//while(ma==31  )	 
    if(ma==13)
    		continue;   	
    
    b=0;
    AB=0;
    b2=0;
  	while(ma==15){ //TEST
  		if(AB==0){
  	   		   AB=1;
  	   		   k=MK1;
    		   m=MR1;
    		   index=(k-m)+2; //第? 行
    	}
     	if(b==0){		
     			W_dspace(0x80);
     			W_DISPh(0x81,2,15,WSE);	
     			b=1;
    		 	W_DISP(0x90,WSE1);
    		    W_DISP(0x88,WSE2); //
  			 	W_DISP(0x98,Wspace); //空白
    	}
    	if(b2==0){	 	
    			b2=1;
    	 		i=0x80;
    	 		if(index==1)
    	 	 		i=0x80; //第一行
    	 		else if(index==2)
    	 	 		i=0x90; //第2行
    	 		else if(index==3)
    	 	 		i=0x88; //第3行	 
    	 		else if(index==4)
    	 	 		i=0x98; //第4行	 
    	 		else;	 
    	 		spi_data(0,0x0F); //display on/ cursor oN
    	 		spi_data(0,i);//位址X
    	 		tep_t10=15000; //30 sec
    	 
      	}
      	delay1ms(30);
      	i=INA000;
      	if((i&0x01)==0){ //搖桿往前
 			        	check_INA000_off(0x01);
 			        	if(index==3){
 			        			index=2;
 			        			b2=0;
 			        			k=1;
 			        	}					
 			        	continue;
 		}      	
 		else if((i&0x02)==0){ //搖桿往後
 			        	check_INA000_off(0x02);
 			        	if(index==2){
 			        			index=3;
 			        			b2=0;
 			        			k=2;
 			        	}					
 			        	continue;
 		} 
 	  	else if((i&0x08)==0){ //搖桿往右 
 				 check_INA000_off(0x08);
 				 	ma=50+k;
 				 	MR1=m;
 				 	MK1=k;
 				 	break;
 	  	}
    	else if((i&0x04)==0 || tep_t10==0){ //搖桿往左 回上一層
    			check_INA000_off(0x04);
    				ma=1;
    				if(tep_t10==0)
    						ma=255;
    				break;
    	}
    	else;
    }//while(ma==15)	
    if(ma==1)
    		continue; 	
    b=0;
  	while(ma==51){  //SENSOR TEST
  			 	if(b==0){	 
  			 		 W_dspace(0x80);
     				 W_DISPh(0x81,2,15,WSE1);	
     				 b=1;
     				 b2=0;	
    				 m=0;
    				 tep_t1=100;	
    		 		 W_DISP(0x90,WSE11); //>>可調整電眼VR  
    		 		 W_DISP(0x98,Wspace); //空白
    		    	 W_DISP(0x88,WB13); //目前=
    		    	 spi_data(0,(0x88+2));
  			 		spi_data(1,0x4C);//L
 					spi_data(1,0x45);//E
 					spi_data(1,0x44);//D
 					spi_data(1,0x3D);//=
  			 		 ot_relay&=0x7f;	//sensor on
					  delay100ms(3);
    	  	      }	//if(b==0)		
    	  	      if(b2==0){
    	  	      		  b2=1;
    	  	      		  if(P35==1){
    	  	      		  		  spi_data(0,(0x88+4));
    	  	      		  		  spi_data(1,0x4f);//O
 								  spi_data(1,0x46);//F
 								  spi_data(1,0x46);//F
 								  spi_data(1,0x20);// 
    	  	      		  		  if(m==0)
    	  	      		  		     W_DISP(0x98,w_ok); //ok
    	  	      		  		   else{
    	  	      		  		   	  W_DISP(0x98,w_ng); //ng
    	  	      		  		   	  W_DISP(0x90,WSE12);//請檢查接線或電眼
    	  	      		  		   	  spi_data(0,0x90);
    	  	      		  		   }	  	     
    	  	      		  }
    	  	      		  else{
    	  	      		  		 spi_data(0,(0x88+4));
    	  	      		  		 spi_data(1,0x4f);//O
 								  spi_data(1,0x4e);//N
 								  spi_data(1,0x20);//
 								  spi_data(1,0x20);// 
    	  	      		  		 if(m==0)
    	  	      		  		 	W_DISP(0x98,w_ng); //ng
    	  	      		  		 else{
    	  	      		  		   	W_DISP(0x98,w_ok); //ok
    	  	      		  		   	W_DISP(0x90,WSE13);//按取物鈕離開
    	  	      		  		 } 
    	  	      		  		 spi_data(0,0x90); 	
    	  	      		  }
    	  	        }
    	  	       if(tep_t1==0){
    	  	       		 tep_t1=100;
    	  	       		 b2=0;
    	  	       		 continue;
    	  	       }			
     			  delay1ms(30);
      	          i=INA000;
 	  	          if((i&0x20)==0 ){ //catch
 				  	 check_INA000_off(0x20);
 				  	 if(m==0){
 				  	 	  m=1;	 
 				  	 	  ot_relay|=0x80;	//sensor off
 				  	 	  delay100ms(2);
 				  	 	  b2=0;
 				  	 	  tep_t1=100;
 				  	 }
 				  	 else{
 				  	    ma=15;
    					break;
    				}
     	         }
    }//while(ma==51)	
    if(ma==15)
    		continue; 	
    b=0;
  	while(ma==52){  //中獎燈測試
  			 	if(b==0){	 
  			 		 //k=ma-50;
  			 		 W_dspace(0x80);
     				 W_DISPh(0x81,2,15,WSE2);	
     				 b=1;
     				 m=0;
    		 		 W_DISP(0x90,WSE21); //按鈕開啟中獎燈
    		 		 W_DISP(0x88,WSE23); //按小卡模式->切換
    		 		 W_DISP(0x98,WSE24); //<<搖桿向左<-離開
    		 		 tep_t10=15000; //30 sec
    		 		 spi_data(0,0x90);//位址X
    	  	    }	//if(b==0)		
    
    	  		delay1ms(30);
      	          i=INA000;
 	  	          if((i&0x20)==0 ){ //catch
 				  	 check_INA000_off(0x20);
 				  	 tep_t10=15000; //30 sec
 				  	 if(m==0){
 				  	 	  m=1;	 
 				  	 	  ot_meter|=0x20; //中獎燈 on 
 				  	 	  W_DISP(0x90,WSE22); //按鈕關閉中獎燈
 				  	 	  spi_data(0,0x88);//位址X
 				  	 	  
 				  	 }
 				  	 else{
 				  	 	 m=0;	 
 				  	 	  ot_meter&=0xdf; //中獎燈 off
 				  	 	  W_DISP(0x90,WSE21); //按鈕開啟中獎燈
 				  	 	  spi_data(0,0x90);//位址X
 				  	 }
 				  	 
     	         }
     	         else if((i&0x04)==0 || tep_t10==0){ //搖桿往左 回上一層
    					check_INA000_off(0x04);
    					ot_meter&=0xdf; //中獎燈 off
    					ma=15;
    					if(tep_t10==0)
    						ma=255;
    					break;
    			}
    			else;
    }//while(ma==52)	
    if(ma==15)
    		continue; 	
    
    
    
    
 } 
 
 
 
 ot_meter&=0xdf; //中獎燈 off

}

//x ccw馬達 啟動
void Xccw_motor_on(unsigned char n )
{
	    ET0=1;
		ot_dip|=0x20; //nrst1 on
 		OT6000=ot_dip;
 		ot_relay|=0x01; //
		OT3000=ot_relay;
		if(n==100){
			PCAPWM2=0x02;  //100%
 			CCAP2H=0;
 		}
 		else{
 			PCAPWM2=0;  //
 			CCAP2H=n+n+20;
 		}
 		//CCAP2L=Fdm_SetSpeed_L;
 		CCAPM2=0x42; //comp=1 & pwm=1	
}
//x cw馬達 啟動
void Xcw_motor_on(unsigned char n)
{
		ET0=1;
		ot_dip|=0x20; //nrst1 on
 		OT6000=ot_dip;
		ot_relay&=0xfe; //
		OT3000=ot_relay;
		if(n>=100){
			PCAPWM2=0x02;  //100%
 			CCAP2H=0;
 		}
 		else{
 			PCAPWM2=0;  //
 			CCAP2H=n+n+20;
 		}
 		//CCAP2L=Bdm_SetSpeed_L;
 		CCAPM2=0x42; //comp=1 & pwm=1	
}
//前後馬達_固定剎車用 b=1前;b=0後
void FB_motor_bk_fix(bit b)
{
		ot_dip|=0x20; //nrst1 on
 		OT6000=ot_dip;
		ot_relay&=0xfe; //
		if(b==1)	//ford
			ot_relay|=0x01; //	
		OT3000=ot_relay;
		PCAPWM2=0;  //BIT9=0
 		CCAP2H=20;
 		//CCAP2L=Bdm_SetSpeed_L;
 		CCAPM2=0x42; //comp=1 & pwm=1	
 		delay1ms(50);
 		//FB_motor_off_s();
}

//X馬達 關閉 b=0 cw ;b=1 ccw;
void X_motor_off(bit b)
{
		X_motor_off_s();
		delay1ms(50);
		FB_motor_bk_fix(b);
		X_motor_off_s();
 		ot_dip&=0xdf; //nrst1 off
 		OT6000=ot_dip;
 		ET0=0;
}

//Z cw馬達 啟動
void Zcw_motor_on(void)
{
		ot_dip|=0x40; //nrst2 on
 		OT6000=ot_dip;
 		ot_relay|=0x02; //
		OT3000=ot_relay;
		PCAPWM3=0x02;  //100%
 		CCAP3H=0;
 		CCAPM3=0x42; //comp=1 & pwm=1	
}
//x ccw馬達 啟動
void Zccw_motor_on(void)
{
		ot_dip|=0x40; //nrst2 on
 		OT6000=ot_dip;
		ot_relay&=0xfD; //
		OT3000=ot_relay;
		PCAPWM3=0x02;  //100%
 		CCAP3H=0;
 		CCAPM3=0x42; //comp=1 & pwm=1	
}
//前後馬達_固定剎車用 b=1前;b=0後
void Z_motor_bk_fix(bit b)
{
		ot_dip|=0x40; //nrst2 on
 		OT6000=ot_dip;
		ot_relay&=0xfD; //
		if(b==1)	//ford
			ot_relay|=0x02; //	
		OT3000=ot_relay;
		PCAPWM3=0;  //BIT9=0
 		CCAP3H=20;
 		CCAPM3=0x42; //comp=1 & pwm=1	
 		delay1ms(50);
}

//Z馬達 關閉 b=0 cw ;b=1 ccw;
void Z_motor_off(bit b)
{
	if(b==1)
		delay100ms(1);
	else
		delay100ms(1);
	Z_motor_off_s();
		//delay1ms(50);
		//Z_motor_bk_fix(b);
		//Z_motor_off_s();
 	ot_dip&=0xbf; //nrst2 off
 	OT6000=ot_dip;
}
//Z馬達 關閉 b=0 cw ;b=1 ccw;
void Z_motor_off_bk(bit b)
{
		Z_motor_off_s();
		delay1ms(50);
		Z_motor_bk_fix(b);
		Z_motor_off_s();
 		ot_dip&=0xbf; //nrst2 off
 		OT6000=ot_dip;
}

//*****************************
//m=步數 n=速度    
void Yspeed_cw( unsigned int m, unsigned char n)    
{
 unsigned int h;
 unsigned char i;
	for(h=0;h<m;h++){  //m*0.056=  mm				
		ot_dip&=0xfb;
		OT6000=ot_dip;
		for(i=0;i<10;i++);		
		ot_dip|=0x04;
		OT6000=ot_dip;
		delayusf(n);	
		y_pos++;
	}
}
    
	
void Y_go_cw(unsigned char n)
{
unsigned char i;	
		ot_dip&=0xfb;
		OT6000=ot_dip;
		for(i=0;i<10;i++);		
		ot_dip|=0x04;
		OT6000=ot_dip;
		delayusf(n);	
		y_pos++;
}
void Y_go_ccw(unsigned char n)
{
   unsigned char i;	
		ot_dip&=0xf7;
		OT6000=ot_dip;
		for(i=0;i<10;i++);		
		ot_dip|=0x08;
		OT6000=ot_dip;
		delayusf(n);
		if(	y_pos>0)
			y_pos--;
}		
    
    
    
void Y_home_org(void)
{
unsigned char i;

	i=IN8000;
	if((i&0x20)!=0){	
		sec_time2=0;
		mtime=10;
		do{
			if(mtime==0)
				error(14);//Y軸或原點異常
			Y_go_ccw(30);	
			i=IN8000;
		}while((i&0x20)!=0);
	}
	y_pos=0;
}	
void Y_home(void)
{
unsigned char i;

	i=IN8000;
	if((i&0x20)!=0){	
		sec_time2=0;
		mtime=10;
		do{
			if(mtime==0)
				error(14);//Y軸或原點異常
				
			if(y_pos>500)
					Y_go_ccw(20);
			else if(y_pos>200)
					Y_go_ccw(50);	
			else 
				Y_go_ccw(80);	
			i=IN8000;
		}while((i&0x20)!=0);
	}
	y_pos=0;
}	


void Y_go_org(void)
{
unsigned char i;
//unsigned int  h;
	i=IN8000;
	if((i&0x20)!=0){
		Y_home_org();
	}
	delay100ms(5);
	Yspeed_cw(1000,20);  //1000步*0.056=56mm ;速度10(越大越小)
	i=IN8000;
	if((i&0x20)==0){
		Yspeed_cw(1000,20);  //1000步*0.056=56mm ;速度10(越大越小)
		i=IN8000;
		if((i&0x20)==0)
					error(14);//Y軸或原點異常
	}
	delay100ms(6);
	Y_home();
}

		

void QC_test(void)
{
unsigned char i,k,ma,m,index;
//unsigned int hh;
 bit b,b2,b3;

	bank_6295(0); 
	ic_6295_ch(0x08,2,0);   //SECOND 1
	check_INE000_off(0x10);
	check_INE000_off(0x08);
	LCM_clr();
    ma=1;
 	while(1){
 		b=0;
 		m=1;
 		k=1;
 		index=2; //第2行
    	while(ma==1){
     		if(b==0){		
     			W_DISP(0x80,Wqctest);
     			b=1;
     			switch(m){ //menu
    		 		case 1:
    		 			W_DISP(0x90,WQA);
    		    		W_DISP(0x88,WQB);
    		 			W_DISP(0x98,WQC);
    					break;
    				case 2:
    		 			W_DISP(0x90,WQB);
    		    		W_DISP(0x88,WQC);
    		 			W_DISP(0x98,WQD);
    					break;
    				case 3:
    		 			W_DISP(0x90,WQC);
    		    		W_DISP(0x88,WQD);
    		 			W_DISP(0x98,WQE);
    					break;
    			}
 		 		i=0x80;
    	 		if(index==1)
    	 	 		i=0x80; //第一行
    	 		else if(index==2)
    	 	 		i=0x90; //第2行
    	 		else if(index==3)
    	 	 		i=0x88; //第3行	 
    	 		else if(index==4)
    	 	 		i=0x98; //第4行	 
    	 		else;	 
    	 		spi_data(0,0x0F); //display on/ cursor oN
    	 		spi_data(0,i);//位址X
    	 
        	}
      		delay1ms(30);
      		i=INA000;
 	  		if((i&0x08)==0){ //搖桿往右 
 				 	check_INA000_off(0x08);
 				 	if(k!=2){
 				 		ma=10+k;
 				 		break;
 				 	}
 	  		}
 	  		else if((i&0x01)==0){ //搖桿往前   	
    			check_INA000_off(0x01);
    			if(k>1){
 			        k--;
 			        if(index>2)
 			        	index--;
 			        else if(index==2){
 			        		if(m>1)
 			        			m--;
 			        }
 			        else;
 			        b=0;		
 			     }
 			     continue;			
      		}
      		else if((i&0x02)==0){ //後 -
    			check_INA000_off(0x02);
    			if(k<5){
 			         k++;
 			        if(index<4)
 			        	index++;
 			        else if(index==4){
 			        		if(m<3)
 			        			m++;
 			        }
 			        else;
 			       b=0; 			
 			     }
 			     continue;
    		}
    		else if((i&0x04)==0 ){ //左 回上一層
    			check_INA000_off(0x04);
    			ma=1;
    			break;
    		}
    	
    		else;
    }	//while(ma==1)
   	b=0;
  	while(ma==11){ //SOUND
  			if(b==0){		
     					W_dspace(0x80);
     					W_DISPh(0x81,2,15,WQA);
 						W_DISP(0x90,WQA11);
     					W_DISP(0x88,WQA12);
     					W_DISP(0x98,WQA13);
     					m=1; //B1 B2
     					k=1; //1~10
     					b=1;
     					b2=1;
     					disp_dec8_set(m,0x90+3);
     					disp_dec8_set(k,0x90+5);
     					spi_data(0,0x98);//位址X
     		}
     		if(b2==0){
     			b2=1;
     			if(m==1)
    				bank_6295(0); 
    			else
    				bank_6295(1); 
				ic_6295_ch(0x08,k,0);   //
			}
     			
 			delay1ms(30);
      		i=INA000;
 	  		if((i&0x08)==0){ //搖桿往右 
 	  			if(m==1)
 	  				m=2;
 	  			else
 	  				m=1;
 	  			disp_dec8_set(m,0x90+3);
 	  			vocice_6295_stop();
 			    check_INA000_off(0x08);
 	  		}
 	  		else if((i&0x01)==0){ //搖桿往前   	
 	  			if(k<10)
 			        k++;
 			    disp_dec8_set(k,0x90+5);
    			check_INA000_off(0x01);
    			b2=0;
      		}
      		else if((i&0x02)==0){ //後 -
      			if(k>1)
 			        k--;
 			    disp_dec8_set(k,0x90+5);
    			check_INA000_off(0x02);
    			b2=0;
    		}
    		else if((i&0x04)==0 ){ //左 回上一層
    			check_INA000_off(0x04);
    			vocice_6295_stop();
    			ma=1;
    			break;
    		}
    		else if((i&0x20)==0 ){ //catch
				check_INA000_off(0x20);
				b2=0;
			}
 	}
 	b=0;
    while(ma==12){ //B:sensor+sw
  		if(b==0){		
     					LCM_clr();
     					W_DISP(0x80,WQB);
     					W_DISP(0x90,WQB11);
     					W_DISP(0x88,WQB12);
     					W_DISP(0x98,WQB13);
     					b=1;
     	}
     	delay1ms(30);
      	i=INA000;
     	if((i&0x04)==0 ){ //左 回上一層
    			check_INA000_off(0x04);
    			ma=1;
    			break;
    	}
    }
    b=0;
    while(ma==13){ //>C : X軸
  		if(b==0){		
     					LCM_clr();
     					W_DISP(0x80,WQC);
     					W_DISP(0x90,WQC11);
     					W_DISP(0x88,WQC12);
     					W_DISP(0x98,WQC13);
     					b=1;
     					k=0x07;
     					m=0x07;
     					index=0;
     					b3=0;
     					Xcountb=0; //x有無歸原點
     	}
     	i=IN8000;
     	k=i&0x07;
     	if(k!=m && b3==0){
     				b3=1;
     				m=k;
     	}	
     	if(b3==1 ){  
    				b3=0;
    				spi_data(0,0x90+2);//位址X
    				if((k&0x01)==0){
     			 	 	 spi_data(1,0x4f);//o
     			 	 	 Xcountb=1;
     			 	 	 x_pos=0;
     			 	 	 disp_int4_set(x_pos,0x88+3);
     			 	 }
     			 	else
     			 		 spi_data(1,0x58);//X	 
     			 	spi_data(1,0x20);// 
     			 	//spi_data(0,0x90+4);//位址X
     			 	//if((k&0x02)==0)
     			 	// 	 spi_data(1,0x4f);//o
     			 	//else
     			 	// 	 spi_data(1,0x58);//X	 	 
     			 	//spi_data(1,0x20);// 
     			 	spi_data(0,0x90+7);//位址X
     			 	if((k&0x04)==0)
     			 	 	 spi_data(1,0x4f);//o
     			 	else
     			 	 	 spi_data(1,0x58);//X	 	 
     			 	spi_data(1,0x20);// 
     			 	
    	}
    	delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ //搖桿往右 
 	  		i=IN8000;
 	  		if((i&0x04)!=0){ //右限
 	  			Xcw_motor_on(80);
 	  			do{
 	  				
 	  				delay1ms(10);
 	  				i=IN8000;
 	  				if((i&0x04)==0) //右限
 	  					break;
 	  				delay1ms(10);
 	  				i=INA000;
 	  			}while((i&0x08)==0); //搖桿往右 
 	  			X_motor_off(0);
 	  			if(Xcountb==1)
 	  				disp_int4_set(x_pos,0x88+3);
 	  		}
 	  		check_INA000_off(0x08);
 	  	}
 	  	else if((i&0x04)==0){ //搖桿往左 
 	  		i=IN8000;
 	  		if((i&0x01)!=0){ //左限
 	  			Xccw_motor_on(80);
 	  			do{
 	  				delay1ms(10);
 	  				i=IN8000;
 	  				if((i&0x01)==0){ //左限
 	  					break;
 	  				}
 	  				delay1ms(10);
 	  				i=INA000;
 	  			}while((i&0x04)==0); //搖桿往左 
 	  			X_motor_off(1);
 	  			if(Xcountb==1)
 	  				disp_int4_set(x_pos,0x88+3);
 	  		}
 	  		check_INA000_off(0x04);
 	  	}	
       else if((i&0x20)==0 ){ // 回上一層
    			check_INA000_off(0x20);
    			Xcountb=0;
    			ma=1;
    			break;
      }
    } //while(ma==13)
    b=0;
    while(ma==14){ //>D : Y軸
  		if(b==0){		
     					LCM_clr();
     					W_DISP(0x80,WQD);
     					W_DISP(0x90,WQD11);
     					W_DISP(0x88,WQD12);
     					W_DISP(0x98,WQD13);
     					b=1;
     					k=0x20;
     					m=0x20;
     					index=0;
     					b3=0;
     					b2=0;
     					//Xcountb=0; //x有無歸原點
     	}
    	i=IN8000;
     	k=i&0x20;
     	if(k!=m && b3==0){
     				b3=1;
     				m=k;
     	}	
     	if(b3==1 ){  
    				b3=0;
    				spi_data(0,0x90+4);//位址X
    				if((k&0x20)==0){
     			 	 	 spi_data(1,0x4f);//o
     			 	 }
     			 	else{
     			 		 spi_data(1,0x58);//X	 
     			 	}
     			 	spi_data(1,0x20);// 
     			 	
    	}
    	delay1ms(30);
      	i=INA000;
    	if((i&0x20)==0 && b2==0 ){ // start
    		Y_go_org();
    		disp_int4_set(y_pos,0x88+3);
    		W_DISP(0x98,WQB13);
    		b2=1;
    		check_INA000_off(0x20);
    	}
    	else if((i&0x02)==0 && b2==1){ //搖桿往後  
    	  if(y_pos<Y_MAX){ 	
    	  	  do{
    			Y_go_cw(Y_Playstep);
    			i=INA000;
    			if((i&0x02)!=0)
    				break;
    		 }while(y_pos<Y_MAX);
    		}
    		disp_int4_set(y_pos,0x88+3);
    		check_INA000_off(0x02);
    	}
    	else if((i&0x01)==0 && b2==1){ //搖桿往前   	
    		i=IN8000;
			if((i&0x20)!=0){  //Y_org
				do{
					Y_go_ccw(Y_Playstep);
    				i=IN8000;
    				if((i&0x20)==0){
    					y_pos=0;
    					break;
    				}
    				i=INA000;
    		 	}while((i&0x01)==0);
    		 	
    	    }
    		disp_int4_set(y_pos,0x88+3);
    		check_INA000_off(0x01);
    	}
    	else if((i&0x04)==0){ //搖桿往左 
    		check_INA000_off(0x04);
    			ma=1;
    			break;
      }
    } //while(ma==14)
    b=0;
    while(ma==15){ //>E : Z軸
  		if(b==0){		
     					LCM_clr();
     					W_DISP(0x80,WQE);
     					W_DISP(0x90,WQE11);
     					W_DISP(0x88,WQE12);
     					W_DISP(0x98,WQB13);
     					b=1;
     					k=0xd0;
     					m=0xd0;
     					index=0;
     					b3=0;
     					b2=0;
     	}
     	i=IN8000;
     	k=i&0xd0;
     	if(k!=m && b3==0){
     				b3=1;
     				m=k;
     	}	
     	if(b3==1 ){  
    				b3=0;
    				spi_data(0,0x90+1);//位址X
    				spi_data(1,0x3a);//:
    				if((k&0x10)==0){
     			 	 	 spi_data(1,0x4f);//o
     			 	 }
     			 	else
     			 		 spi_data(1,0x58);//X	 
     			 	spi_data(0,0x90+4);//位址X
     			 	spi_data(1,0x3a);//:
     			 	if((k&0x40)==0)
     			 	 	 spi_data(1,0x4f);//o
     			 	else
     			 	 	 spi_data(1,0x58);//X	 	 
     			 	//spi_data(0,0x90+7);//位址X
     			 	//spi_data(1,0x3a);//:
     			 	//if((k&0x80)==0)
     			 	// 	 spi_data(1,0x4f);//o
     			 	//else
     			 	 //	 spi_data(1,0x58);//X	 	 
     			 	
    	}
    	delay1ms(30);
      	i=INA000;
      	if((i&0x20)==0  ){ // start
      		if(b2==0)
      			W_DISP(0x88,WQE12B);
      		b2=1;
      		
      	}
      	else{
      		if(b2==1)
      			W_DISP(0x88,WQE12);
      		b2=0;	
      	}
 	  	if((i&0x01)==0 ){ //搖桿往前 
    	     if(b2==1 ){ // start
    	     	 do{
					Y_go_ccw(Y_Playstep+100);
    				i=IN8000;
    				if((i&0x20)==0){
    					y_pos=0;
    					break;
    				}
    				i=INA000;
    		 	 }while((i&0x01)==0);
    		 }
    	     else{	 
 	  		
 	  				i=IN8000;
 	  				if((i&0x10)!=0){ //z1
 	  					Zccw_motor_on();
 	  					do{
 	  				
 	  						delay1ms(30);
 	  						i=IN8000;
 	  						if((i&0x10)==0 ) //z1 
 	  							break;
 	  						delay1ms(20);
 	  						i=INA000;
 	  					}while((i&0x01)==0); //搖桿往前 
 	  					Z_motor_off(1);
 	  				}
 	  			}
 	  		    check_INA000_off(0x01);
 	  	}
 	  	else if((i&0x02)==0){ //搖桿往後  
 	  		if(b2==1 ){ // start
 	  				do{
    					Y_go_cw(Y_Playstep+100);
    					i=INA000;
    					if((i&0x02)!=0)
    						break;
    		 		}while(y_pos<Y_MAX);
    		 }
 	  		else{
 	  		
 	  				i=IN8000;
 	  				tep_t10=400;
 	  				if((i&0x40)!=0){ //z2
 	  					Zcw_motor_on();
 	  					do{
 	  						delay1ms(30);
 	  						i=IN8000;
 	  						if((i&0x40)==0 ){ //z2 
 	  							Z_motor_off_bk(0);
 	  							break;
 	  						}
 	  						else if((i&0x10)==0 && tep_t10==0){ //z1 轉一圈
 	  							Z_motor_off(0);
 	  							break;
 	  						}
 	  						delay1ms(20);
 	  						i=INA000;
 	  					}while((i&0x02)==0); //搖桿往後
 	  					Z_motor_off_bk(0);
 	  				}
 	  		}
 	  		check_INA000_off(0x02);
 	  	}	
       else if((i&0x04)==0){ //搖桿往左 
       	   if(b2==1){ // start
       	   		i=IN8000;
 	  			if((i&0x01)!=0){ //左限
 	  				Xccw_motor_on(30);
 	  				do{
 	  					delay1ms(10);
 	  					i=IN8000;
 	  					if((i&0x01)==0){ //左限
 	  						break;
 	  					}
 	  					delay1ms(10);
 	  					i=INA000;
 	  				}while((i&0x04)==0); //搖桿往左 
 	  				X_motor_off(1);
 	  			}
 	  		}
 	  		else{
 	  			check_INA000_off(0x04);
    			ma=1;
    			break;
    		}
       	   check_INA000_off(0x04);
       	   
    		
      }
      else if((i&0x08)==0){ //搖桿往右 
      	  if(b2==1 ){ // start
 	  		i=IN8000;
 	  		if((i&0x04)!=0){ //右限
 	  			Xcw_motor_on(30);
 	  			do{
 	  				
 	  				delay1ms(10);
 	  				i=IN8000;
 	  				if((i&0x04)==0) //右限
 	  					break;
 	  				delay1ms(10);
 	  				i=INA000;
 	  			}while((i&0x08)==0); //搖桿往右 
 	  			X_motor_off(0);
 	  		}
 	  	 }
 	  	 check_INA000_off(0x08);
 	  	
 	  }
      
      
      
      
    } //while(ma==15)		
    		
	
 	
 } //while(1)
}

void lcm_disp_head(void)
{
  //unsigned char i,j;		
  
     LCM_clr();
    W_DISP(0x80,w_thank_you); //THANK YOU
 	W_DISP(0x98,Wtitle_1);//第4行 程式版本
 	
 	delay100ms(10);
}
void z_home(void)
{
  unsigned char i;
  bit b1=0;	
	i=IN8000;
 	if((i&0x10)!=0){ //z1 原點
 		Zccw_motor_on();
 		tep_t10=1000;//5秒
 		do{
 	  				
 	  		delay1ms(30);
 	  		i=IN8000;
 	  		if((i&0x40)==0){ //z2
 	  					b1=1;
 	  					break;
 	  		}
 	  		if(tep_t10==0){
 	  			Z_motor_off(1);
 	  			error(16);// z軸歸原點異常
 	  		}
 	  			
 	  		i=IN8000;
 	  	}while((i&0x10)!=0 ); //z1 
 	  	Z_motor_off(1);
 	  	if(b1==1){
 	  		Zcw_motor_on();
 	  		tep_t10=1000;//5秒
 	  		do{
 	  				delay1ms(30);
 	  				if(tep_t10==0){
 	  					Z_motor_off(0);
 	  					error(16);// z軸歸原點異常
 	  				}
 	  				i=IN8000;
 	  		}while((i&0x10)!=0 ); //z1 
 	  		Z_motor_off(0);
 	  	}
	}//if((i&0x10)!=0){ //z1 原點
}
void x_home(unsigned char n)
{
  unsigned char i;
  //bit b1=0;	
	
	tep_t10=4000;//20秒
	i=IN8000;
 	if((i&0x01)!=0){ //左限
 	  	Xccw_motor_on(n);
 	  	do{
 	  			delay1ms(30);
 	  			if(tep_t10==0){
 	  				X_motor_off(1);
 	  				error(10);// X軸歸原點異常
 	  			}
 	  			//delay1ms(20);
 	  			i=IN8000;
 	  	}while((i&0x01)!=0); //左限
 	  	X_motor_off(1);
 	}
 	//x_pos=0;

}
void x_home_good(void)
{
  unsigned char i,j;
  //bit b1=0;	
	
	tep_t10=2000;//10秒
	i=IN8000;
 	if((i&0x01)!=0){ //左限
 		j=100;
 		if(x_pos<50)
 			j=30;
 		else if(x_pos<100)
 			j=70;
 	  	Xccw_motor_on(j);
 	  	do{
 	  			delay1ms(10);
 	  			j=100;
 				if(x_pos<50)
 					j=30;
 				else if(x_pos<100)
 					j=70;
 	  			Xccw_motor_on(j);
 	  			if(tep_t10==0){
 	  				X_motor_off(1);
 	  				error(10);// X軸歸原點異常
 	  			}
 	  			//delay1ms(20);
 	  			i=IN8000;
 	  	}while((i&0x01)!=0); //左限
 	  	X_motor_off(1);
 	}
 	x_pos=0;

}

void x_check(void)
{
  //unsigned char i;
  //bit b1=0;	
		Xcw_motor_on(80);
		tep_t10=300;//1.5秒
 	  	do{
 	  				
 	  			delay1ms(30);
 	  			/*
 	  			i=IN8000;
 	  			if((i&0x04)==0){ //x2
 	  				X_motor_off(0);
 	  				error(12);
 	  			}
 	  			if(tep_t10==0){
 	  				X_motor_off(0);
 	  				error(11);// x1
 	  			}	 
 	  			i=IN8000;*///x1
 	  	 }while(tep_t10!=0); 
 	  	 X_motor_off(0);
 	  	 disp_int3_set(x_pos,0x98+6);
 	  	 delay100ms(5);	
 	  	 if(x_pos<40)
 	  			 error(13);
 	  	 //delay100ms();	
 	  	 x_home_good();
}


void joystick(void)
{
 unsigned char stp,joy;
 stp=IN8000;
 joy=INA000;
 
 	if((joy&0x04)==0){ //搖桿往左
		if((stp&0x01)!=0 ){ //原點sw off
			if(ffd!=0){
				ffd++;
				if(ffd>50){
					ffd=0;
					Xccw_motor_on(100);
				}
				else if(ffd>40)
					Xccw_motor_on(85);
				else if(ffd>30)
					Xccw_motor_on(65);
				else 
					Xccw_motor_on(50);
			}
			else
				Xccw_motor_on(100);
	    }
	    else{ 
	    	X_motor_off(1);
	    	x_pos=0;
	    	ffd=1;
	     	ffb=1;
	    }
	    ffb=1;
	    RLf=1;
  	}
  	else if((joy&0x08)==0){ //搖桿往右
	    if((stp&0x04)!=0 ){ //右限 sw off
	    	if(ffb!=0){
				ffb++;
				if(ffb>50){
					ffb=0;
					Xcw_motor_on(100);
				}
				else if(ffb>40)
					Xcw_motor_on(85);
				else if(ffb>30)
					Xcw_motor_on(65);
				else 
					Xcw_motor_on(50);
			}
			else
				Xcw_motor_on(100);
	    }
	    else{ //停sw off
			X_motor_off(0);
			ffd=1;
	     	ffb=1;
		}
		ffd=1;
		RLf=2;
	}
	else{
		if(RLf==1){ 
	    	X_motor_off(1);
	    	ffd=1;
	     	ffb=1;
	    }
	    else if(RLf==2){ 
	    	X_motor_off(0);
	    	ffd=1;
	     	ffb=1;
	    }
	    RLf=0;
	}
}
	

void disp_Ncoin(unsigned int n)
{		
  unsigned char i;	
  bit b=0;	
    spi_data(0,0x91);//cursor 第2行
	hex_dec(n);
	//spi_data(1,0x20);//
	if(hexdec[3]==0)
			i=0x20;
			
	else{
       i=0x30+hexdec[3];
       b=1;
   }
 	spi_data(1,i);//
	if(b==0 && hexdec[2]==0)
			i=0x20;
	else{
       i=0x30+hexdec[2];
       b=1;
   }
 	spi_data(1,i);//
 	if(b==0 && hexdec[1]==0)
			i=0x20;
	else
       i=0x30+hexdec[1];
 	spi_data(1,i);//
 	i=0x30+hexdec[0];
 	spi_data(1,i);//
 	
}		
void disp_Ntime(unsigned char n)
{		
  unsigned char i;		
    spi_data(0,0x96);//cursor 第2行
	hex_dec8(n);
    i=0x30+hexdec[1];
 	spi_data(1,i);//
 	i=0x30+hexdec[0];
 	spi_data(1,i);//
		
}	


	
/*
void Gnum_disp(unsigned char m)
{
	unsigned char i,j,k,p,n;
	hex_dec8(m);
	n=0;
	if(hexdec[2]==0){
		if(hexdec[1]==0)
			n=2;
		else
			n=1;
	}
	
	spi_data(0,0x36);//function set RE=1
 	delayus(50);
	for(k=0;k<=2;k++){  //0,1,2
		p=hexdec[2-k];
 		if(k==0 && n!=0)
 				p=10;
 		else if(k==1 && n==2)
 				p=10;		
 		for(j=0;j<=19;j++){  //0,1,2
 				spi_data(0,0x85+j);//繪圖位址Y
 				spi_data(0,0x8d+k);//繪圖位址X
 				
 				i=W_NUM[p][j*2];
 				spi_data(1,i);//
 				i=W_NUM[p][j*2+1];
 				spi_data(1,i);//
 		}
 	}
 	delayus(50);
 	//spi_data(0,0x36);//function set RE=1 G啟動繪圖模式
 	//delayus(10);
 	spi_data(0,0x30);//function set RE=0
}*/
/*
void GTIME_disp(void)
{
unsigned char i,j,k;
	spi_data(0,0x36);//function set RE=1
 	delayus(50);
	for(j=0;j<=19;j++){  //TIME:
 		spi_data(0,0x85+j);//繪圖位址Y
 		spi_data(0,0x88);//繪圖位址X
 		for(k=0;k<=4;k++){
 				i=W_TIME[j][k*2];
 				spi_data(1,i);//
 				i=W_TIME[j][k*2+1];
 				spi_data(1,i);//
 		}
 	}
 	delayus(50);
 	spi_data(0,0x30);//function set RE=0
 	Gnum_disp(playtime);
}*/
/*
void GPLAY_disp(void)
{
unsigned char i,j,k;
	spi_data(0,0x36);//function set RE=1
 	delayus(50);
	for(j=0;j<=19;j++){  //TIME:
 		spi_data(0,0x85+j);//繪圖位址Y
 		spi_data(0,0x88);//繪圖位址X
 		for(k=0;k<=4;k++){
 				i=W_PLAY[j][k*2];
 				spi_data(1,i);//
 				i=W_PLAY[j][k*2+1];
 				spi_data(1,i);//
 		}
 	}
 	delayus(50);
 	spi_data(0,0x30);//function set RE=0
 	Gnum_disp(play);
 	disp_Ncoin();
}
	*/
	
//造字"投""幣"時"間"
void make_Wcoin(void)
{
  unsigned char a,i,j;
  for(i=0;i<=3;i++){
  		for(j=0;j<=31;j++){
  		  	a=wb5_coin[i][j];	  	
  		  spi_data(1,a);
   		}
  }			
}	
	
//待機顯示畫面
void lcm_init(void)
{
  unsigned char i,j;		
    LCM_clr();
 	spi_data(0,0x40);//CG RAM 1
 	make_Wcoin(); //造字"投""幣"時"間"
 	
 	spi_data(0,0x81);//cursor 第一行
 	spi_data(1,0x00);//
 	spi_data(1,0x00);//顯示造字1
 	spi_data(1,0x00);//
 	spi_data(1,0x02);//顯示造字2
 	
 	spi_data(0,0x85);//cursor 第一行
 	spi_data(1,0x00);//
 	spi_data(1,0x04);//顯示造字3
 	spi_data(1,0x00);//
 	spi_data(1,0x06);//顯示造字4
 	
 	grafic_clear();//圖形GDRAM 歸零
 
 	for(j=0;j<=1;j++){
 		spi_data(0,0x80+j);//繪圖位址Y
 		spi_data(0,0x80);//繪圖位址X
 		for(i=0;i<=7;i++){
 			spi_data(1,0xff);//
 			spi_data(1,0xff);//
 		}	
 	}	
 	
 	for(j=2;j<=29;j++){
 		spi_data(0,0x80+j);//繪圖位址Y
 		spi_data(0,0x80);//繪圖位址X
 		spi_data(1,0xc0);//
 		spi_data(1,0x00);//
 		
 		spi_data(0,0x80+j);//繪圖位址Y
 		spi_data(0,0x84);//繪圖位址X
 		spi_data(1,0xc0);//
 		spi_data(1,0x00);//
 		
 		spi_data(0,0x80+j);//繪圖位址Y
 		spi_data(0,0x87);//繪圖位址X
 		spi_data(1,0x00);//
 		spi_data(1,0x03);//
 	}	
 	for(j=0;j<=1;j++){
 		spi_data(0,0x9e+j);//繪圖位址Y
 		spi_data(0,0x80);//繪圖位址X
 		for(i=0;i<=7;i++){
 			spi_data(1,0xff);//
 			spi_data(1,0xff);//
 		}	
 	}	
 	
 	delayus(100);
 	spi_data(0,0x36);//function set RE=1 G啟動繪圖模式
 	delayus(10);
 	spi_data(0,0x30);//function set RE=0
 	disp_Ncoin(credit_int);
 	disp_Ntime(0);
 	
 	
}


void meter_chk(void)//斷表檢查
{
	unsigned char i;
	i=INE000;
	if(bcoin1_m==0){		//coin1 跳表
 		if((i&0x20)!=0 ){  //段表1
 			meter_n[0]++;
 			if(meter_n[0]>=6)
 				 error(1);
 		}
 		else
 			meter_n[0]=0;
 	}
 	if(bcoin2_m==0){		//coin2 跳表
 		if((i&0x40)!=0 ){  //段表2
 			meter_n[1]++;
 			if(meter_n[1]>=6)
 				 error(2);
 		}
 		else
 			meter_n[1]=0;
 	}	
	if(out_m==0){		//out  跳表
 		if((i&0x80)!=0 ){  //段表1
 			meter_n[2]++;
 			if(meter_n[2]>=6)
 				 error(3);
 		}
 		else
 			meter_n[2]=0;
 	}
	
}	

void  credit_1(void)
{
	//unsigned char i;	
	if(play>0)
  			play--;
  	bank_add_b=0;
  	bank_fp_b=0;
  	if(credit_int>=coin1_play)
  	{	
	   credit_int-=coin1_play;
	   //play=credit;
	   coin_save_ram();
	   bank_add_b=1; //有扣錢要加銀行
	   //coin_play_M[0]++; //遊戲一開始銀行先加在zone A
		//Ramint_save(0x0070,coin_play_M[0]);
	   //game_bank_M[0]++;
	   //Ramint_save(0x0090,game_bank_M[0]);
	   if(win_modM!=0 ){ //促銷
	   	   if(TSnowNO<TSno_M ){
	   			TSnowNO++;
	   			fm25writ_data8(0x002d,TSnowNO);
	   		}
	   	}
	   
	}   
	else if(freeplay_M==1){
		bank_fp_b=1; //免費遊戲 ，取消 ，有扣錢要加銀行
		
	}
}
// X走m步(不包含已走的步數)
void Xcw_go(unsigned int m)
{
 unsigned char i,j;
 signed int k;	
 i=IN8000;
 if((i&0x04)!=0 ){ //右限 sw off
 	 m=m+x_pos;
 	 do{
 	 	k=m-x_pos;
 	 	if(k<=0)
 	 		break;
 		if(k>200)
 			j=100;
 		else if(k>100)
 			j=50;
 		else if(k>30)
 			j=15;
 		else
 			j=8;	
 		Xcw_motor_on(j);
 		delay1ms(30);
 		i=IN8000;
 	}while((i&0x04)!=0 );
 	X_motor_off(0);
 }
}
// Y走m步(不包含已走的步數)  , n=減速
void Ycw_go(unsigned int m,unsigned char n)
{
  //unsigned char i,j;	
	
	if(y_pos<Y_MAX){ 
	   m=m+y_pos;	
       do{
    			Y_go_cw(Y_Playstep+n);
    			if(y_pos>=m)
    			 	break;
    	}while(y_pos<Y_MAX);
    }
}

// Y走m步(不包含已走的步數), n=減速
void Yccw_go(unsigned int m,unsigned char n)
{
  unsigned char i;	
	i=IN8000;
	if((i&0x20)!=0){
		if(y_pos>m){ 
	  		 m=y_pos-m;	
       		do{
    			Y_go_ccw(Y_Playstep+n);
    			if(y_pos<=m)
    			 	break;
    			i=IN8000;
    		 }while((i&0x20)!=0);
    	}
    }
}

//return=1 失敗
unsigned char push_gun(void)
{	
  unsigned char i;
 tep_t10=400;
 i=IN8000;
 if((i&0x40)!=0){ //z2
 	  	Zcw_motor_on();
 	  	tep_t11=1000; //5 秒
 	  	while(1){
 	  		delay1ms(30);
 	  		i=IN8000;
 	  		if((i&0x40)==0){ //z2 失敗
 	  					Z_motor_off_bk(0);
 	  					return(1);
 	  		}
 	  		else if((i&0x10)==0 && tep_t10==0 ){ //z1 轉一圈ok
 	  					Z_motor_off(0); 
 	  					return(0);
 	  		}
 	  		
 	  		if(tep_t11==0)
 	  			error(17);
		}
 }
 return(0);
}

void gun_up(void)
{
  unsigned char i;
	
		i=IN8000;
 		if((i&0x10)!=0){ //z1 原點
 			Zccw_motor_on();
 			tep_t10=1600;//8秒
 			do{
 	  			delay1ms(30);
 	  			if(tep_t10==0){
 	  				Z_motor_off(1);
 	  				error(16);// z軸歸原點異常
 	  			}
 	  			i=IN8000;
 	  		}while((i&0x10)!=0 ); //z1 
 	  		Z_motor_off(1);
    	}
}

//以x_pos,y_pos 為基底 載入初值
void load_xy_init(void)
{
 unsigned char i,j;	
 unsigned int x,y;	
	x=x_pos;
	y=y_pos;
	for(i=0;i<=2;i++){
		for(j=0;j<=4;j++){
			if(i==1)
				lineX[i][j]=(X_2xstep*j)+X_xstep+x;
			else	
				lineX[i][j]=(X_2xstep*j)+x;
		}
	}
	for(j=0;j<=4;j++){
		for(i=0;i<=2;i++){
			if(i==1)
				lineY[i][j]=Y_ystep+y;
			else
				lineY[i][j]=(Y_ystep*i)+y;
		}
	}
}
//手動調整搖桿	x軸最大左移50步與右移50步
//y軸最大前移1000步與後移1000步
void adj_go(void)	
{	
 unsigned char i;	
 unsigned int x_hi,x_lo,y_hi,y_lo;	
 
 x_hi=x_pos+50;
 if(x_pos<=50)
 	 x_lo=0;
 else
 	x_lo=x_pos-50;
 y_hi=y_pos+1000;
 if(y_hi>=Y_MAX)
 	 y_hi=Y_MAX;
 if(y_pos<=1000)
 	 y_lo=0;
 else
 	y_lo=y_pos-1000;
 W_DISP(0x90,Wmov_adj); // >>MOVE--> Adj. 
 spi_data(0,0x90);//位址X
 spi_data(0,0x0F); //display on/ cursor oN
 
 while(1){
    	delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ //搖桿往右 
 	  		i=IN8000;
 	  		if((i&0x04)!=0){ //右限
 	  			if(x_pos<x_hi){
 	  				Xcw_motor_on(8); //速度20%
 	  				do{
 	  					delay1ms(20);
 	  					i=IN8000;
 	  					if((i&0x04)==0) //右限
 	  						break;
 	  					if(x_pos>=x_hi)
 	  						break;
 	  					i=INA000;
 	  				}while((i&0x08)==0); //搖桿往右 
 	  				X_motor_off(0);
 	  				disp_int4_set(x_pos,0x88+3);
 	  				spi_data(0,0x90);//位址X
 	  			}
 	  		}
 	  		check_INA000_off(0x08);
 	  	}
 	  	else if((i&0x04)==0){ //搖桿往左 
 	  		i=IN8000;
 	  		if((i&0x01)!=0){ //左限
 	  			if(x_pos>x_lo){
 	  				Xccw_motor_on(8);
 	  				do{
 	  					delay1ms(20);
 	  					i=IN8000;
 	  					if((i&0x01)==0) //左限
 	  						break;
 	  					if(x_pos<=x_lo)
 	  						break;	
 	  					i=INA000;
 	  				}while((i&0x04)==0); //搖桿往左 
 	  				X_motor_off(1);
 	  				disp_int4_set(x_pos,0x88+3);
 	  				spi_data(0,0x90);//位址X
 	  			}
 	  		}
 	  		check_INA000_off(0x04);
 	  	}	
    	else if((i&0x02)==0 ){ //搖桿往後,大  
    	  if(y_pos<y_hi){ 	
    	  	  do{
    			Y_go_cw(Y_Playstep+150);
    			i=INA000;
    			if((i&0x02)!=0)
    				break;
    		  }while(y_pos<y_hi);
    		  disp_int4_set(y_pos,0x98+3);
    		  spi_data(0,0x90);//位址X
    	   }
    	   check_INA000_off(0x02);
    	}
    	else if((i&0x01)==0 ){ //搖桿往前,小   	
    		i=IN8000;
			if((i&0x20)!=0){  //Y_org
				if(y_pos>y_lo){ 
					do{
						Y_go_ccw(Y_Playstep+150);
    					i=IN8000;
    					if((i&0x20)==0){
    						y_pos=0;
    						break;
    					}
    					if(y_pos<=y_lo)
    						break;
    					i=INA000;
    		 		}while((i&0x01)==0);
    		 	}
    		 	disp_int4_set(y_pos,0x98+3);
    		 	spi_data(0,0x90);//位址X
    	    }
    		
    		check_INA000_off(0x01);
    	}
    	else if((i&0x20)==0 ){ //catch
    		i=push_gun();
    		if(i==0)  //ok
    			break;
    		else{
    			delay100ms(5);
    			gun_up();
    		}
    	}
   }
    				
}
//這是最準的嗎?
void XY_readj(bit bb )
{
  unsigned char i;
  bit b=0;
  W_DISP(0x80,W_xyready); //這個位置最準嗎?
  W_DISP(0x90,Wyesno);//>>YES   >>NO
  spi_data(0,0x0F); //display on/ cursor oN
  spi_data(0,0x90+1);	
  while(1){
    	delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ //搖桿往右 
 	  		if(b==0){
 	  			spi_data(0,0x90+5);
 	  			b=1;
 	  		}
 	  		check_INA000_off(0x08);
 	  	}
 	  	else if((i&0x04)==0){ //搖桿往左 
 	  		if(b==1){
 	  			spi_data(0,0x90+1);
 	  			b=0;
 	  		}
 	  		check_INA000_off(0x04);
 	  	}	
 	  	else if((i&0x20)==0 ){ //catch
 	  		check_INA000_off(0x20);
 	  		if(b==0){  //不用重調
 	  			W_DISP(0x90,Wsave);//
 	  			spi_data(0,0x90);
 	  			voice_dodo(1);
 	  			if(bb==0)
 	  				W_DISP(0x80,W_tochadj); //A: Machine adj.
 	  			else{
 	  				W_DISP(0x80,W_tochchk); //C: Machine chk.	
 	  				W_DISP(0x90,Wxy_chk); // X . Y chk.<  > 
 	  			}
 	  			break;
 	  		}
 	  		else{ //需要重調
 	  			voice_dodo(3);
 	  			if(bb==0)
 	  				W_DISP(0x80,W_tochadj); //A: Machine adj.
 	  			else
 	  				W_DISP(0x80,W_tochchk); //C: Machine chk.	
 	  			adj_go();
 	  			W_DISP(0x80,W_xyready); //這個位置最準嗎?
  				W_DISP(0x90,Wyesno);//>>YES   >>NO
  				spi_data(0,0x0F); //display on/ cursor oN
  				spi_data(0,0x90+1);
  				
  				b=0;
  				continue;
 	  		}
    		
    		
    	}
    }	

}
// xy 調整與確認
void XY_adj(bit bs)  //bs==0 adj ;bs==1 chk
{
 unsigned char i,p,r,sps;
 unsigned int x,y,chknm;	
 bit b0=0,b1=0,bb=0,spsb=0;
 	LCM_clr();
 	
 	if(bs==0)
 		W_DISP(0x80,W_tochadj); //A: Machine adj.
 	else	
		W_DISP(0x80,W_tochchk); //C: Machine chk.
	W_DISP(0x88,wmach_init); // Machine Initial 
	voice_dodo(2);
	z_home();
    delay100ms(1);
    Y_go_org();
    delay100ms(1);
    x_home(80);
    Xcountb=1;
    x_pos=0;
    delay100ms(3);
    x_check();
    delay100ms(3);
    p=1;
    bb=0;
    chknm=0;
  if(bs==0){  
    W_DISP(0x88,W_xyinit);////座標回復初始值 ?
    W_DISP(0x98,Wnoyes);//>>NO    >>YES
    spi_data(0,0x0F); //display on/ cursor oN
    spi_data(0,0x98+1);
    
    while(1){
    	delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ //搖桿往右 
 	  		if(p==1){
 	  			p=2;
 	  			spi_data(0,0x98+5);
 	  			bb=1;
 	  		}
 	  		check_INA000_off(0x08);
 	  	}
 	  	else if((i&0x04)==0){ //搖桿往左 
 	  		if(p==2){
 	  			p=1;
 	  			spi_data(0,0x98+1);
 	  			bb=0;
 	  		}
 	  		check_INA000_off(0x04);
 	  	}	
 	  	else if((i&0x20)==0 ){ //catch
 	  		if(bb==1){
 	  			W_DISP(0x98,Wsave);//
 	  			voice_dodo(3);
 	  			
 	  		}
    		check_INA000_off(0x20);
    		break;
    	}
    }
    
    spi_data(0,0x0c); //display on/off/ cursor off
}
 while(1){   
    b0=0;
    b1=0;
	i=lineXY_load(); //回復0= OK ; 1= 需定位，資料不回初值; 2= 需定位，資料回初值 
	if(bs==1){
		if(i!=0)
			error(9);//定位資料錯誤
		i=fm25read_data8(0x0010); //0X99==OK
		if(i!=0x99){
				fm25writ_data8(0x0010,0);
				error(9);//定位資料錯誤
		}
		W_DISP(0x90,Wxy_chk); // X . Y chk.<  > 
		disp_int4_set(chknm,0x90+5);
	}
	else{	
		if(i!=0 || bb==1){
			if(i==2 || bb==1){  //載入第一點資料初值
				b0=1; //資料要全部更新
				lineX[0][0]=X1_init;
				lineY[0][0]=Y1_init;
			}
			bb=0;
		}
		W_DISP(0x90,Wxy_adj); // X . Y Adj.<  > 
	}
  	  W_DISP(0x88,WQC12);//X_pos:
      W_DISP(0x98,WQD12);//Y_pos:
  	  for(p=0;p<=2;p++){ //for(p=0;p<=2;p++){ 拍片用
  	  	  
  	  	  	for(r=0;r<=4;r++){
  	  	  	  if(x_pos>=lineX[p][r]){
  	  	  	  	  fm25writ_data8(0x0010,2); ////定位資料錯誤，請重新定位
  	  	  	  	  error(9); //定位資料錯誤，請重新定位
  	  	  	  }
  	  	  	  else
  	  	  	  	  x=lineX[p][r]-x_pos;
  	  	  	  Xcw_go(x); //
      		  disp_int4_set(x_pos,0x88+3);
      		  delay100ms(5);
      		  if(lineY[p][r]>y_pos){
      		  	  y=lineY[p][r]-y_pos;
      		  	  if(y>200)
      		  	  	  i=20;
      		  	  else
      		  	  	  i=200;
      		  	  Ycw_go(y,i); //
      		  }
      		  else if(lineY[p][r]<y_pos){
      		  	  y=y_pos-lineY[p][r];
      		  	  if(y>200)
      		  	  	  i=20;
      		  	  else
      		  	  	  i=200;
      		  	  Yccw_go(y,i); //
      		  }
      		  disp_int4_set(y_pos,0x98+3);
	  		  delay100ms(5);
      		  i=push_gun();
      		  if(i==0){  //ok
      		  	 if(bs==0){ 
      		  	    XY_readj(0); //這是最準的嗎? 0=adj
      	  			if(b0==1 && b1==0){ //全部載入初值
      	  	  			load_xy_init();
      	  	  			W_DISP(0x80,W_tochadj); //A: Machine adj.
      	  	  			save_XYdata();
      	  	  			b1=1;
      	  	  		}
      	  	  		else{
      	  	  			lineX[p][r]=x_pos;
						lineY[p][r]=y_pos;
						save_lineX_single(p,r);
						save_lineY_single(p,r);
					}
				}
				else{
						spi_data(0,0x90+4);
						spi_data(1,0x4f); //o
						spi_data(1,0x20);
				}
      	  	  			
      		  }
      		  else{ //ng 手動調整
      		  	  if(bs==0){ 
      		  			fm25writ_data8(0x0010,0);  
    	  				delay100ms(5);
    	  				gun_up();
      	  				adj_go();
      	  				XY_readj(0); //這是最準的嗎? 0=adj
      	  				if(b0==1 && b1==0){//全部載入初值
      	  	  					load_xy_init();
      	  	  					W_DISP(0x80,W_tochadj); //A: Machine adj.
      	  	  					save_XYdata();
      	  	  					b1=1;
      	  	  			}
      	  	  			else{
      	  	  					lineX[p][r]=x_pos;
								lineY[p][r]=y_pos;
								save_lineX_single(p,r);
								save_lineY_single(p,r);
						}
					}
					else{
						//spi_data(0,0x90+4);
						//spi_data(1,0x58); //x
						//spi_data(1,0x20);
						W_DISP(0x80,Werror_09);//X.Y 定位資料錯誤
      					W_DISP(0x90,Werror_09a);//請重新校正>>X.Y
      					disp_int3_set(chknm,0x88+6);
						tep_t1=250;	
    					sps=5;
    					spsb=0;
						while(1){
							delay1ms(50);
							if(tep_t1==0  ){
								tep_t1=250;	
    							if(spsb==0){
    								if(sps==0x04)
    									sps=0x05;
    								else
    									sps=0x04;
    							}	
    							spi_data(0,0x36);//function set RE=1 G啟動繪圖模式
 								delayus(50);
 								spi_data(0,sps);//function反白
 								spsb=~spsb;
 								spi_data(0,0x30);//function set RE=0
 							}
	    					i=INA000;
	  						if((i&0x20)==0)
 									break;
 							
 						}
 						//delay100ms(5);
 						if(spsb==1){
 							delayus(100);
 							spi_data(0,0x36);//function set RE=1 G啟動繪圖模式
 							delayus(50);
 							spi_data(0,sps);//function反白
 							spsb=~spsb;
 							spi_data(0,0x30);//function set RE=0
 						}
    	  				gun_up();
      	  				adj_go();
      	  				XY_readj(1); //這是最準的嗎? 1=chk
      	  	  			lineX[p][r]=x_pos;
						lineY[p][r]=y_pos;
						save_lineX_single(p,r);
						save_lineY_single(p,r);
					}
						
						
      	  	 }
      	  	 if(bs==0){
      	  	 	spi_data(0,0x0c); //display on/off/ cursor off
      	  	 	delay100ms(5);
      	  	 	W_DISP(0x90,Wxy_adj); // X . Y Adj.<  > 
      	  	}
      	  	else
      	  		delay100ms(5);
      	  	 
      	  	 if(p==1){
      	  	 	 if(r==3)
      	  	 	 	 break;
      	  	 }
      	 }
      	  	
      	  x_home_good();
      	  Y_home();
      	  delay100ms(5);
      }
   if(bs==0){   
   	  save_XYdata(); 
      fm25writ_data8(0x0010,0x99); ////定位ok
      W_DISP(0x88,Wspace);//
      W_DISP(0x98,WOK);// ok
      bank_6295(0); 
	  ic_6295_ch(0x08,2,0);   //SECOND 1	 
	  delay100ms(10);
	  W_DISP(0x90,Wpsh_togame); // >>PUSH-->To Game
	  W_DISP(0x88,Wpsh_Re_adj); //>>PUSH-->RE Adj.
	  spi_data(0,0x90);//位址X
 	  spi_data(0,0x0F); //display on/ cursor oN
 	  p=1;
 	  while(1){
 	  	  	delay1ms(30);
	  		i=INA000;
	  		if((i&0x20)==0)
	  			break;
	  	    else if((i&0x01)==0){
	  	    	if(p==2){
	  	    		p=1;
	  	    		spi_data(0,0x90);//位址X
	  	    	}
	  			check_INA000_off(0x01);
	  		}
	  		else if((i&0x02)==0){
	  	    	if(p==1){
	  	    		p=2;
	  	    		spi_data(0,0x88);//位址X
	  	    	}
	  			check_INA000_off(0x02);
	  		}
	  	}
	  	check_INA000_off(0x20);
	  	ic_6295_ch(0x08,2,0);   //SECOND 1	 
	    delay100ms(5);	
	}
	else{
		W_DISP(0x80,wchk_ok); //~ OK ~請重新開機
		chknm++;
		disp_int4_set(chknm,0x90+5);
		ic_6295_ch(0x08,2,0);   //SECOND 1	 
	    delay100ms(10);
	    /*
	    do{
	    	delay1ms(50);
	    	i=INA000;
	  	}while((i&0x20)!=0);*/
	    W_DISP(0x80,W_tochchk); //C: Machine chk.
	}
 	if( bs==0 &&  p==1)
 	  		break;
 } //while(1)
	  
}	  

/*
void XY_check(void)
{
 unsigned char i,p,r;
 unsigned int x,y;	
 	LCM_clr();
	W_DISP(0x80,Wxy_adj); //
	W_DISP(0x88,wmach_init); //
	z_home();
    delay100ms(1);
    Y_go_org();
    delay100ms(1);
    x_home(70);
    Xcountb=1;
    x_pos=0;
    delay100ms(3);
    x_check();
    delay100ms(3);
  while(1){  
    p=0x41;
    x=X1_init;
    y=Y1_init;
    for(r=0;r<=8;r++){
    	spi_data(0,0x80+6);//位址X
    	spi_data(1,p+r);
    	spi_data(1,'1');
    	W_DISP(0x90,WQC12);//X_pos:
    	W_DISP(0x88,WQD12);//Y_pos:
    	if((r&0x01)==0){
    		
    		Xcw_go(x); //
    		disp_int4_set(x_pos,0x90+3);
    	//if(r==8){
    		delay100ms(5);	
    		Ycw_go(y,0); //
    		disp_int4_set(y_pos,0x88+3);
    		delay100ms(5);
    		i=push_gun();
    		if(i==1)
    			while(1);
    		delay100ms(5);
    		spi_data(0,0x80+6);//繪圖位址X
    		spi_data(1,p+r);
    		spi_data(1,'2');
    		y=Y_ystep+Y_ystep;
    		Ycw_go(y,0); //
    		disp_int4_set(y_pos,0x88+3);
    		delay100ms(5);
    		i=push_gun();
    		if(i==1)
    			while(1);
    		delay100ms(5);
//}
    	}
    	else{
    		Xcw_go(x); //
    		
    		disp_int4_set(x_pos,0x90+3);
    		delay100ms(5);	
    		y+=Y_ystep;
    		Ycw_go(y,0); //
    		disp_int4_set(y_pos,0x88+3);
    		delay100ms(5);
    		i=push_gun();
    		if(i==1)
    			while(1);
    		delay100ms(5);
    	}
    	Y_home();
    	delay100ms(5);
    	x=X_xstep;	
    	y=Y1_init;
    }
    x_home_good();
    delay100ms(5);
 }
}
    		
/*	
void XYZ_TEST(void)
{
unsigned char i,j;

	W_DISP(0x88,wmach_init); //
 	W_DISP(0x98,Wspace);//
	z_home();
    delay100ms(1);
    Y_go_org();
    delay100ms(1);
    x_home(70);
    Xcountb=1;
    x_pos=0;
    delay100ms(3);
    x_check();
    W_DISP(0x90,WQC12);//X_pos:
    W_DISP(0x88,WQD12);//Y_pos:
    W_DISP(0x98,Wspace);//
    W_DISP(0x80,Wxyztest);//
  while(1){
    
    delay1ms(30);
      	i=INA000;
 	  	if((i&0x08)==0){ //搖桿往右 
 	  		i=IN8000;
 	  		if((i&0x04)!=0){ //右限
 	  			Xcw_motor_on(100);
 	  			do{
 	  				
 	  				delay1ms(30);
 	  				i=IN8000;
 	  				if((i&0x04)==0) //右限
 	  					break;
 	  				delay1ms(20);
 	  				i=INA000;
 	  			}while((i&0x08)==0); //搖桿往右 
 	  			X_motor_off(0);
 	  			if(Xcountb==1)
 	  				disp_int4_set(x_pos,0x90+3);
 	  		}
 	  		check_INA000_off(0x08);
 	  	}
 	  	else if((i&0x04)==0){ //搖桿往左 
 	  		i=IN8000;
 	  		if((i&0x01)!=0){ //左限
 	  			Xccw_motor_on(100);
 	  			do{
 	  				delay1ms(30);
 	  				i=IN8000;
 	  				if((i&0x01)==0){ //左限
 	  					break;
 	  				}
 	  				delay1ms(20);
 	  				i=INA000;
 	  			}while((i&0x04)==0); //搖桿往左 
 	  			X_motor_off(1);
 	  			if(Xcountb==1)
 	  				disp_int4_set(x_pos,0x90+3);
 	  		}
 	  		check_INA000_off(0x04);
 	  	}	
    	else if((i&0x02)==0 ){ //搖桿往後,大  
    	  if(y_pos<Y_MAX){ 	
    	  	  do{
    			Y_go_cw(Y_Playstep+5);
    			i=INA000;
    			if((i&0x02)!=0)
    				break;
    		 }while(y_pos<Y_MAX);
    		}
    		disp_int4_set(y_pos,0x88+3);
    		check_INA000_off(0x02);
    	}
    	else if((i&0x01)==0 ){ //搖桿往前,小   	
    		i=IN8000;
			if((i&0x20)!=0){  //Y_org
				do{
					Y_go_ccw(Y_Playstep+5);
    				i=IN8000;
    				if((i&0x20)==0){
    					y_pos=0;
    					break;
    				}
    				i=INA000;
    		 	}while((i&0x01)==0);
    		 	
    	    }
    		disp_int4_set(y_pos,0x88+3);
    		check_INA000_off(0x01);
    	}
    	else if((i&0x20)==0 ){ //catch
 	  		tep_t10=400;
 	  		i=IN8000;
 	  		if((i&0x40)!=0){ //z2
 	  			Zcw_motor_on();
 	  			tep_t11=1000; //5 秒
 	  			while(1){
 	  				delay1ms(30);
 	  				i=IN8000;
 	  				if((i&0x40)==0 || (i&0x80)==0){ //z2 || z3
 	  					break;
 	  				}
 	  				else if((i&0x10)==0 && tep_t10==0){ //z1 轉一圈
 	  					break;
 	  				}
 	  				if(tep_t11==0)
 	  					error(17);
 	  			}
 	  			Z_motor_off(0);
    		}
    		else if((i&0x10)!=0){ //z1
 	  				Zccw_motor_on();
 	  				tep_t11=1000; //5 秒
 	  				while(1){
 	  					delay1ms(30);
 	  					i=IN8000;
 	  					if((i&0x10)==0 ) //z1 
 	  						break;
 	  					if(tep_t11==0)
 	  						error(16);
 	  				}
 	  				Z_motor_off(1);
 	  		 }
 	  		  else error(18);
    		
    		check_INA000_off(0x20);
    	}
    		
	}
}
	*/
	
	
	
//計算中獎率與中獎位置
void count(void)
{
  unsigned char i,j,m,n;
  unsigned int k,h;
  bit bx,by,bz;
  play_X=0;
  play_Y=0;
  h=lineY[1][0]-lineY[0][0];
  h=h/2;
  k=lineY[1][0]-h;
  if(y_pos>k){
  	  k=lineY[2][0]-h;
  	  if(y_pos>k)
  	  	  play_Y=2;
  	  else
  	  	  play_Y=1;
  }
  else
  	  play_Y=0;
  h=lineX[play_Y][1]-lineX[play_Y][0];
  h=h/2;
  play_X=0;
  if(play_Y!=1){
  	  for(j=4;j>=1;j--){
  	  		k=lineX[play_Y][j]-h;
  	  		if(x_pos>k){
  	  			play_X=j;
  	  			break;
  	  		}
  	  	}
  	}
  	else{
  	  	for(j=3;j>=1;j--){
  	  		k=lineX[play_Y][j]-h;
  	  		if(x_pos>k){
  	  			play_X=j;
  	  			break;
  	  		}
  	  	}
   }
   
  bx=0;
  by=0;	  
  k=lineX[play_Y][play_X];
  h=lineY[play_Y][play_X];	
  if(gift_money_okb==1){	//選務金額已滿額
   	   if( x_pos>=(k-4) && x_pos<=(k+4) ){  //可以啟動偷跑
   	   	     if( y_pos<(h-10)  && y_pos>=(h-200)){
   	   	     	 j=h-y_pos;
   	   	     	 while(j!=0){
   	   	     	 	Y_go_cw(Y_Playstep+10); //自動修正;往前修正
   	   	     	 	j--;
   	   	     	}
   	   	     }
   	   	     
   	   	     else if( y_pos>(h+10)  && y_pos<=(h+100)){
   	   	     	 j=y_pos-h;
   	   	     	 while(j!=0){
   	   	     	 	Y_go_ccw(Y_Playstep+20); //自動修正;往後修正
   	   	     	 	j--;
   	   	     	}
   	   	     }
   	   	 }
   }
   else{
  		if( x_pos>=(k-X_gost) && x_pos<=(k+X_gost) ){
  	  			bx=1;
  				if( y_pos>=(h-Y_gost) && y_pos<=(h+Y_gost) ){
  						k=(h+Y_gost)-y_pos; ////偷跑步數
  						by=1;
  				}
  		}
  
  		bz=0; //要干擾
  		//j=play_Y;
  		//if(j>=2)
  	  	//	j=2;
  	  	/*
  		if(game_bank_M>=0){ //正數才計算，負數直接干擾
  	  	  	h=game_bank_M;
  	  		if(h>=payout_M)
  	  	  			bz=1;  //不干擾
   		}*/
   		m=random;
   		if(win_modM!=0 && bank_add_b==1){ //促銷 + 有實際扣錢
   			if(TS_out_M==0){ //未促銷
   				 
   				 if(TSnowNO>=TSno_M )
						bz=1;
						
				 else{
			  			n=TSno_M-TSnowNO;
			  			if(n>80){
			  	  			if((m&0x1f)==0x0f)
			  		  		  bz=1;
			  			}		
			  			else if(n>50){
			  	  			if((m&0x1f)==0x0f)
			  		  		  bz=1;
			  			}		
			  			else if(n>30){
			  	  			if((m&0x0f)==0x07)
			  		  		  bz=1;
			  			}		
			  			else if(n>15){
			  	  			if((m&0x07)==0x03)
			  		  		  bz=1;
			  			}		
			  			else if(n>5){
			  	  			if((m&0x03)==0x01)
			  		  		  bz=1;
			  			}		
			 			else{
			  				if((m&0x01)==0x01)
			  		  		  bz=1;
			 			}
			 	  }
			 	  if(bz==1){
			 				TS_out_M=1;////已促銷
			 				fm25writ_data8(0x002c,TS_out_M);
			 				//spi_data(0,0x88+7);//繪圖位址X
   							//spi_data(1,0x41);
   							//spi_data(1,0x42);
			 	  }
   		
   	          }
   	    }
  		if(bz==0 && bx==1 && by==1){ //進行干擾
  	  			i=Y_gost+Y_gost+10;
  	  			j=k;
  	  			while(k!=0 && k<=i){	  
  	 				Y_go_cw(Y_Playstep-10);//偷跑
  	 				k--;
  	 				win_b=1; //有干擾
  	   			}
  	   
  	  
   		}
   	}
   /*
   spi_data(0,0x80+5);//繪圖位址X
   spi_data(1,0x30+play_X);
   spi_data(1,0x30+play_Y);
   if(win_b==1)
   		disp_dec8_set(j,0x80+7);*/
}	


void gmoney_ddc(void) //累加金額扣除選務金額
{
  unsigned int hh;	
	hh=payout_M; //禮品售價   元
    if(gmoney_M>=hh)
    		gmoney_M-=hh;
    else
    		gmoney_M=0;
	Ramint_save(0x0040,gmoney_M);
}

void disp_gift_price(void)
{		
    		W_DISP(0x88,w_gift_price); //禮品售價   元	
    		disp_int4_set(payout_M,0x8c);
    		if(gift_dsp_b==1 ){
     			W_DISP(0x98,w_money_add);  //累加金額    元	  
     			disp_int4_set(gmoney_M,0x9C);	//累加金額
     		}
     		else{
     			W_DISP(0x98,Wwelcome);//歡迎光臨!請投幣
     		} 
     			
}     
void giftmoney_count(void)	//計算選務金額是否已滿額?
{
	if(gift_money_okb==0 ){
    	if(credit_int>=payout_M){
    		gift_money_okb=1;	//選務金額已滿額
    		if(gmoney_addb==0){
    			gmoney_addb=1;
    			gmoney_M+=payout_M;	//累加金額
				Ramint_save(0x0040,gmoney_M);
			}
    			
    		W_DISP(0x98,wgift_money_ok);//已到達選物金額
    	}	
	}
}
void checkdsp_coin(bit b)
{
 		if(coinsp==1){	//顯示投幣金額
  	  	  		coin_save_ram();
  	  	  		if(b==1)
  	  	  			giftmoney_count(); //計算選務金額是否已滿額?
    			disp_Ncoin(credit_int);
    			coinsp=0;
    	}	
    	if(play!=0 && credit_int<coin1_play)
    		disp_Ncoin(play);
}
void gift_dsp_mode(void)	//啟動選物販賣顯示功能
{		
    gift_time_b=0;
    gift_dsp_b=0; // 
   	if(gmoneyDspTime_M==0xff) //永久顯示
    				gift_dsp_b=1; // 必須顯示
    else if	(gmoneyDspTime_M!=0){	
    				gmoneydsptsec=gmoneyDspTime_M*60;	//(顯示保留時間)
    				gift_dsp_b=1; // 必須顯示
    				gift_time_b=1;
    }
}


void sensor_check_onoff(bit b)
{
  //unsigned char i;		
   LCM_clr();
   W_DISP(0x80,Werror);
   W_DISP(0x90,Werror_cd); 	   
   spi_data(0,0x95);//cursor 第1行
   spi_data(1,0x30); //
   spi_data(1,0x36); //
   W_DISP(0x88,Werror_06);
   W_DISP(0x98,Werror_06a);
	bank_6295(1); 
 	while(1){
  		ic_6295_ch(0x08,7,0);   // alm
  		delay100ms(6);
  		if(b==1){
  				if(P35==1)
  					break;
  		}
  		else{
  				if(P35==0)
  					break;
  		}			
  		delay100ms(6);
  		if(b==1){
  				if(P35==1)
  					break;
  		}
  		else{
  				if(P35==0)
  					break;
  		}
  	}
  	lcm_init();
  	
  	
  			
 }	


void sensor_check(void)
{
 unsigned char j;
 bit b;		
    LCM_clr();
 	W_DISP(0x90,Wsensor_test); //-Sensor Check-
	ot_relay&=0x7f;	//sensor on
	delay100ms(3);
	j=0;
	b=0;
	do{
		delay1ms(50);
			j++;
			if(j>=60){
					b=1;
					break;
			}
    }while(P35==0);
    if(b==1)
    	sensor_check_onoff(1);
    ot_relay|=0x80;	//sensor off
	j=0;
	b=0;
	do{
		delay1ms(50);
			j++;
			if(j>=60){
					b=1;
					break;
			}
    }while(P35==1);
    if(b==1)
    	sensor_check_onoff(0);	
  
  W_DISP(0x88,w_ok); //~ OK ~

}

void  GOT_prize(void) 
{      	
unsigned char i,j;
unsigned int hh;	
        	outmeter++;
        	//TSnow_rst(); //促銷歸零--中獎促銷不能歸零
        	game_bank_M=0; //直接歸零
    		Ramint_save(0x0090,game_bank_M);
			gift_out_M++;
			Ramint_save(0x0080,gift_out_M);
        	if(gift_money_okb==1){	//選務金額已滿額
	   					gift_money_okb=0;
	   					hh=payout_M; //禮品售價   元
    					if(credit_int>=hh)
    						     credit_int-=hh;
    					else
    						     credit_int=0;
    					coin_save_ram();
    					disp_Ncoin(credit_int);
    		}		
        	gmoney_ddc();////累加金額扣禮品金額
        	fm25writ_data8(0x0007,0x02);//02=中獎
			gmoney_addb=0;
			ot_meter|=0x20; //中獎燈 on
        	winlightsec=30;
        	winlightb=1;
        	fm25writ_data8(0x001f,0x00); //	完成遊戲流程
        	bank_6295(1);
			ic_6295_ch(0x08,5,0);   //you got it
        	wait_6295(1);
        	ic_6295_ch(0x08,6,0);   //中獎音
             j=0;
             do{
             	 delay1ms(20);
             	 i=check_6295(1);
        	 	 if(i==0){
        	 	 	bank_6295(1); 
   					ic_6295_ch(0x08,8,0);   //winner
   					break;
   				}
   				j++;
   			}while(j<=250);
}

void main(void)
{
  unsigned char i,j,k,m,sp,mk;
  unsigned int hh;
  bit b1,b2,b3;
  bit mub,bb,spsb;
  	 //PCON2=0x81; //oscin/2 82FE564 震盪22.1MHZ/2**81
  	 PCON2=0x80;//oscin 82FE564 震盪22.1184m
 	 AUXR= 0x00;  //
 	 CMOD=0; //1/12 FOSC
 	 STRETCH=0x3F;// & READ WRITE 降速
 	 //P2M0=0x00; //
 	// P2M1=0x03;// P21 P20 =push pull output =>LCM 	已有75245
 	 //P44 P45 可使用
	i=AUXRA_Read();
	i&=~0x20;								//disable OCDE, 當P44, P45變成GPIO後, 將無法進行debug模擬
	AUXRA_Write(i);
 	 OutputInital();
 	 random=fm25read_data8(0x0006);
 	 if(random==0){
 	 		 random=0xbd;
 			 fm25writ_data8(0x0006,random);	
 	 		 
 	 }
 	 timer_init();
	
 	check_ram_init();
 	ram_data_load();
 	
    delay100ms(5);
 	ot_relay|=0x80;	//sensor off
 	CR=1; //start PCA
 	grafic_clear();//圖形GDRAM 歸零
 	lcm_disp_head();
    delay100ms(2);
    i=INC000;
    if((i&0x02)==0) //set2
    		setting();
    		
    	
    j=bo_adjM*4; //Y軸干擾補正值
 	Y_gost=Y_gost0+j;	
    i=INC000;
   if((i&0x01)==0){ //TEST
   		//XYZ_TEST();
   		//XY_check();
   		XY_adj(0);
    }	
    
    i=INE000;
	if((i&0x18)==0){ //qc test
		QC_test();
		
	}		
	else if((i&0x10)==0){ //xy _check
		XY_adj(1); //chk
		
	}	
	
	i=lineXY_load();
	if(i!=0){
			error(9);//定位資料錯誤
	}
	i=fm25read_data8(0x0010); //0X99==OK
	if(i!=0x99){
				fm25writ_data8(0x0010,0);
				error(9);//定位資料錯誤
	}
		
    coin_testb=0;
    W_DISP(0x88,wmach_init); //
 	W_DISP(0x98,Wspace);//
    
    z_home();
    delay100ms(1);
    Y_go_org();
    delay100ms(1);
    x_home(80);
    Xcountb=1;
    x_pos=0;
    delay100ms(3);
    x_check();
    
    W_DISP(0x98,w_ok);//
    
    sensor_check();
    gift_dsp_mode();	//啟動選物販賣顯示功能
    //playcoin=coin1_play;	//每局??元];
    
    check_credit();	 // 取出投幣數
 	//if((catchcoin_M&0x0f)==0)	
    //    	credit_00();
    
 	i=fm25read_data8(0x001f);
 	if(i==0x1f){
 		if(credit_int>=payout_M)
 			credit_00();
 		game_bankM_0();
 		gmoney_00();////累加金額歸零
		fm25writ_data8(0x001f,0x00); //選物滿額記憶關機重開,會歸零投幣金額及累加金額
		fm25writ_data8(0x0007,0x03);//03=不正常關機
		j=data8_ram_read(0x0008); //不正常關機次數
		if(j<255)
				j++;
		data8_ram_save(0x0008,j);
    	  				
    }
    if(credit_int>=payout_M)
    	gmoney_addb=1;
    		
    ot_meter|=0x80;//coin power on
    delay100ms(3);
 	coin_enb=1;
 	for(i=1;i<=2;i++)
    	meter_n[i]=0;  //斷表檢查
 	bb=0;
 	mtime=0;
 	test_play_b=0;
    free_play_b=0;
    if(freeplay_M==1){
    	free_play_b=1;
    	st_lightb=1; //開始紐閃爍
    }
    else if(freeplay_M==2)
    	test_play_b=1;
 	mub=0;
 	mk=1;
 	attmusic_min=0;
 	m=attmusic_M;	
 		
    while(1){
    	if(et1_err!=0)
  	  	  				error(et1_err);	
    	checkdsp_coin(0); //檢查顯示投幣金額
    	while(play==0 && credit_int<coin1_play ){
    			delay1ms(30);
    			if(et1_err!=0)
  	  	  				error(et1_err);	 
  	  	  		meter_chk();
  	  	  		if(bb==0 && mtime==0 ){	
  	  	  					lcm_init();
  	  	  					if(test_play_b!=0){//自動測試機台
 									W_DISP(0x88,WD3);//耐久測試
 									W_DISP(0x98,Wsec_st);////遊戲  秒後開始
 									sec_time=0;
    		    					playtime=playtime_M;
    		    					disp_Ntime(playtime);
 									disp_dec8_set(playtime,(0x98+2));
    						} 		   
    						else if(free_play_b!=0){//免費遊戲
 			   						W_DISP(0x88,WD2);//免費遊戲
 			   						W_DISP(0x98,Wpsh_st);//play star
    						} 	
    						else{
    							disp_gift_price();
    							k=1;
 							}		
  	  	  					
    						bb=1;
    						tep_t1=250;
    						sp=5;	
    						spsb=0;
    						
    				}		
					checkdsp_coin(0); //檢查顯示投幣金額
  	  	  			if(tep_t1==0 && freeplay_M==0 && bb==1){
 							if(spsb==0){
    							if(sp==0x04)
    								sp=0x05;
    							else
    								sp=0x04;
    						}	
    						spi_data(0,0x36);//function set RE=1 G啟動繪圖模式
 							delayus(50);
 							spi_data(0,sp);//function反白
 							spsb=~spsb;
 							spi_data(0,0x30);//function set RE=0
    						
    						if(spsb==0 && sp==0x05){
    							lcm_init();
  	  	  						disp_Ncoin(credit_int);
    								if(k==1){
    										k=2;
    										W_DISP(0x88,Wwelcome);//歡迎光臨!請投幣
 			   								W_DISP(0x98,w_1play);//一投  元每局  元	
 			   								disp_dec8_set(coin1_set,(0x98+2));//
 					 						disp_dec8_set(coin1_play,(0x98+6));//
    								}
    								else{
    									k=1;
    									disp_gift_price();
    								}
    								
    							 
    						}    
    						 if(spsb==1)
    							tep_t1=100;	
    						else
    							tep_t1=250;	
    								
 				  }
 				 
    			i=INC000;
    			if((i&0x01)==0){ //free play
 					play++;
 					disp_Ncoin(play);
 					check_INC000_off(0x01);
 					//free_b=1;
				}
 				else if((i&0x02)==0 ){  //setting
    				vocice_6295_stop();
    				st_lightb=0;
        			ot_meter&=0xf7;	// st light off
    				setting();
  	  				coin_testb=0;
  	  				j=bo_adjM*4; //Y軸干擾補正值
 					Y_gost=Y_gost0+j;	
    				//if((catchcoin_M&0x0f)==0)	
        			//		credit_00();
        			mub=0;	
        			mk=1;
    				attmusic_min=0;
 					m=attmusic_M;	
 					test_play_b=0;
    				free_play_b=0;
    				if(freeplay_M==1){
    					free_play_b=1;
    					st_lightb=1; //開始紐閃爍
    				}
    				else if(freeplay_M==2)
    						test_play_b=1;
 					
 					gift_dsp_mode();	//啟動選物販賣顯示功能
 					ot_meter|=0x80;//coin power on
 					delay100ms(3);
 					coin_enb=1;	
    				bb=0;
    				continue;	
    			}	
    			else;
    			
    			if(test_play_b!=0 ){
      	   				 if(bb==1){	
      	   					if(sec_b==1){
    							disp_Ntime(playtime);
    							disp_dec8_set(playtime,(0x98+2));
    							sec_b=0;
    						}
    						if(playtime==0)	
      	   					   play++;
      	   				 }
      	   		}
    			else if(free_play_b==1){
 				 	 
					i=INA000;
					if((i&0x20)==0){	
						play++;
						disp_Ncoin(play);
						ic_6295_ch(0x08,1,0);   //COIN
						check_INA000_off(0x20);
					}
				}
    		
    			if(attmusic_M!=0){
    				if(mub==0){	//attmusic	
      	   					if(attmusic_min>=60){
      	   						attmusic_min=0;
      	   						m--;
      	   						if(m==0){
      	   								mub=1;
      	   								mk=1;
      	   								bank_6295(1);
  										ic_6295_ch(0x08,3,0);	//demo music
      	   						}
      	   					}
      	   			}
      	   			else{	//attmusic
      	   					if(mk<3){ //attmusic
      	   						i=check_6295(1);
      	   						if(i==0){
  									ic_6295_ch(0x08,3,0);	//demo music
    								mk++;
    							}
    						}
    						else{
      	   						i=check_6295(1);
 								if(i==0){
 										mub=0;
 										mk=1;
 										attmusic_min=0;
    									m=attmusic_M;
    									
    							}
    						}
    				}
    		 }
 					
    	}
    	//**************** Play Start **********************
  	  	  		
    	//vocice_6295_stop();
    	winlightb=0;
  		ot_meter&=0xdf; //中獎燈 off
  		ot_relay&=0x7f;	//sensor on
  		if(credit_int>=coin1_play) //如果有投幣遊戲,freeplay=0;
    			play=0;
    	x_home_good();
    	lcm_init();
        mub=0;
        fm25writ_data8(0x0006,random);	
        bank_6295(0); 
		ic_6295_ch(0x08,3,0);   //遊戲音樂
		gift_money_okb=0;	//選務金額已滿額
        
        //delay100ms(1);
        j=0;
        do{
        	delay1ms(50);
        	if(P35!=0)
        		break;
        	j++;
        	if(j==20)
        		W_DISP(0x88,Wsensor_test); //-Sensor Check-
        }while(j<100);
        if(P35==0){
        	sensor_check();
        	bank_6295(0); 
        	ic_6295_ch(0x08,3,0);   //遊戲音樂
        }
        
        sec_time=0;
        sec_b=0;
        if(freeplay_M==0){ //正常營業
    				W_DISP(0x88,Wstart);//口口遊戲開始口口
    				W_DISP(0x98,Wpsyouwin);//!!祝您中獎!!
    				if( credit_int>=coin1_play){
    					if(gmoney_addb==0){
    						 hh=payout_M; //禮品售價   元
    						if((gmoney_M+coin1_play)>=hh){
    								gift_money_okb=1;	//選務金額已滿額
    								W_DISP(0x98,wgift_money_ok);//已到達選物金額
    								gmoney_addb=1;
    								gmoney_M+=coin1_play;	//累加金額
									Ramint_save(0x0040,gmoney_M);
    								credit_int+=hh;
    								credit_int-=coin1_play;
    								coin_save_ram();
    								disp_Ncoin(credit_int);
    								coin_play_M++; //
  	  	  							Ramint_save(0x0070,coin_play_M);
  	  	  							game_bank_M+=coin1_play;
	      							Ramint_save(0x0090,game_bank_M);
    								
    						}		
    					}
    					giftmoney_count(); //計算選務金額是否已滿額?
    				}
    		}
    		else if(freeplay_M==1){
    					W_DISP(0x88,WD2);//>>免費遊戲
    					W_DISP(0x98,Wpsyouwin);//!!祝您中獎!!
    		}
    		else if(freeplay_M==2){
    						W_DISP(0x88,WD3);//>>機台耐久測試
    						W_DISP(0x98,Wspace);//	
    		}
        
        
        
        playtime=playtime_M;
        disp_Ntime(playtime);
        //GTIME_disp();
         do{
        		delay1ms(20);	
        		if(play!=0 && credit_int<coin1_play){
        			i=INC000;
    				if((i&0x01)==0){ //free play
 						play++;
 						disp_Ncoin(play);
 						check_INC000_off(0x01);
 					}
 				}
        		checkdsp_coin(1); //檢查顯示投幣金額
        		i=check_6295(1);
        		if(i==0)
   					ic_6295_ch(0x08,3,0);   //遊戲音樂
   					
   				
        		i=INA000;
         }while((i&0x0c)==0x0c  );//左或右移動
         st_lightb=1;
         playsb=1;//開始計時
         RLf=0;
         ffd=1;
	     ffb=1;
         b1=0;
         
         do{
         	 joystick();
         	 delay1ms(20);	
        	 checkdsp_coin(1); //檢查顯示投幣金額
        	 i=check_6295(1);
        	 if(i==0)
   					ic_6295_ch(0x08,3,0);   //遊戲音樂
   			if(sec_b==1){
   				//Gnum_disp(playtime);
   				disp_Ntime(playtime);
   				sec_b=0;
   				if(playtime<=5){
							ic_6295_ch(0x10,4,0);   //SECOND 1 ch2
				}
			}
			
			i=INA000;
			if((i&0x20)==0){
				
				if(RLf==1){ 
	    			X_motor_off(1);
	    		}
	    		else if(RLf==2){ 
	    			X_motor_off(0);
	    		}	
	    		RLf=0;
				play_pushb=1;//推桿燈熄滅
				//ic_6295_ch(0x10,9,0);   //按鈕音
				ic_6295_ch(0x08,5,0);   //遊戲音樂 往前
				push_off_b=0;
	    		ckpush=0;
	    		check_push_b=1;
	    		b2=0;
				do{
    				Y_go_cw(Y_Playstep-10);
    				if(push_off_b==1){ //放開按鈕
    					if(y_pos<Y1_check){
    						ic_6295_ch(0x08,3,0);   //遊戲音樂
    						play_pushb=0;
    						Y_home();
    						playtime+=1;
    						if(playtime>playtime_M)
    							playtime=playtime_M;
    						b2=1; //短按按鈕
    						break;
    					}
    					else
    						break;
    				}
    			}while(y_pos<Y1_check);
    			if(b2==0)
    				break;
    		}
    		b2=0;
    		if( playtime==0 )
				b1=1;//時間到
   						
         }while(b1==0  );//push start
         play_pushb=1;//推桿燈熄滅
         if(b1==1){
				if(RLf==1)
	    			X_motor_off(1);
	    		else if(RLf==2)
	    			X_motor_off(0);
	    		RLf=0;
	    		ic_6295_ch(0x08,5,0);   //遊戲音樂 往前
		}
	     st_lightb=0;
        ot_meter|=0x08; //開始燈
        playsb=0;//
 		
        
        if(gift_money_okb==0 ){
        	credit_1(); //遊戲扣錢
        	if(bank_add_b==1 && freeplay_M==0){ //有扣錢要加銀行
        				gmoney_M+=coin1_play;	//累加金額
						Ramint_save(0x0040,gmoney_M);
						coin_play_M++; //
  	  	  				Ramint_save(0x0070,coin_play_M);
  	  	  				game_bank_M+=coin1_play;
	      				Ramint_save(0x0090,game_bank_M);
    		}  
    		else if(bank_fp_b==1){//free play
  				//game_bank_M+=coin1_play;
	      		//Ramint_save(0x0090,game_bank_M);
  			}
    	}
    	disp_Ncoin(credit_int);
    	disp_Ntime(0);
	    push_off_b=0;
	    ckpush=0;
	    check_push_b=1;
	    win_b=0;
	    tep_t1=10;
	    b1=0;
    	do{
    				if(tep_t1==0){
    					tep_t1=10;
    					i=check_6295(1);
        	 			if(i==0)
   							 ic_6295_ch(0x08,5,0);   //遊戲音樂 往前
   					}
    				Y_go_cw(Y_Playstep-10);
    				if(push_off_b==1){ //放開按鈕
    					if(freeplay_M==0  ){ //正常遊戲 or 免費遊戲 才干擾 (免費遊戲不干擾)
    							count(); //計算中獎率與中獎位置+干擾
    							b1=1;
    					}
    					break;
    				}
    	}while(y_pos<Y_MAX); // y_pos<Y_MAX
    	checkdsp_coin(0); //檢查顯示投幣金額	
    	//GPLAY_disp();
    	if(freeplay_M==0 || freeplay_M==1 ){ //正常遊戲 or 免費遊戲 
	    	if(b1==0)//沒有經過count();無法計算 play_X && play_Y
	    		play_Y=2;
	    
  		}
  		disp_Ntime(0);
  		if(win_modM!=0 ){ //促銷 
   				 if(TSnowNO>=TSno_M ){
   				 	 TSnow_rst();
   				 	 
   				 }
   		}
   				 	 
        	delay100ms(2);
        	st_lightb=0;
        	ot_meter&=0xf7;	// st light off
        	ic_6295_ch(0x08,6,0);   //遊戲音樂 下爪
        	b1=0;
        	b2=0;
        	gift_sensor_onb=0;
       	 	gift_sensor_chk=1;//開始偵測gift sensor
       	 	//if(gift_money_okb==1)	//選務金額已滿額
       	 	fm25writ_data8(0x001f,0x1f); //	未完成遊戲流程關機，會清除中獎率
        	Zcw_motor_on();
        	play_pushb=0;//推桿燈亮
        	tep_t10=350;
        	tep_t11=2000; //10 秒
 	  		do{
 	  				delay1ms(30);
 	  				i=IN8000;
 	  				if((i&0x40)==0 ){ //z2
 	  					Z_motor_off_bk(0);
 	  					b1=1; //不中獎
 	  					//break;
 	  				}
 	  				else if((i&0x10)==0 && tep_t10==0){ //z1 轉一圈
 	  					Z_motor_off(0);
 	  					b2=1; //中獎
 	  					//break;
 	  				}
					if(tep_t11==0)
						 error(17);//z軸
			}while(b2==0 && b1==0  );
        	
        
        delay100ms(2);
        if(b1==1){  //不中獎
        	ic_6295_ch(0x08,7,0);   //失敗音
        	
        	W_DISP(0x88,Wgameover);//遊戲結束
        	if(gift_money_okb==0)
        		W_DISP(0x98,Wnextime);//歡迎您再次光臨
        }
        else if(b2==1){  //中獎
        	W_DISP(0x88,Wgameover);//遊戲結束
        	W_DISP(0x98,Wconyouwin);//!!恭喜中獎!!
        	bank_6295(1);
        	ic_6295_ch(0x08,4,0);   //pass音
        	wait_6295(1);
        }
        delay100ms(2);
        //z_home
        i=IN8000;
 		if((i&0x10)!=0){ //z1 原點
 			Zccw_motor_on();
 			tep_t10=1600;//8秒
 			do{
 	  			delay1ms(30);
 	  			if(tep_t10==0){
 	  				Z_motor_off(1);
 	  				error(16);// z軸歸原點異常
 	  			}
 	  			i=IN8000;
 	  		}while((i&0x10)!=0 ); //z1 
 	  		Z_motor_off(1);
    	}
    	
        delay100ms(1);
        if(b2==1){ //中獎
        	j=0;
        	do{
        		j++;
        		delay1ms(50);
        		if(gift_sensor_onb==1)
        			break;
        	}while(j<40);
        }
        b3=0;
        //if( gift_sensor_onb==1 && b2==1  ){ //要進洞才感應
        if( gift_sensor_onb==1  ){ //不須進洞
        	GOT_prize();
        	b3=1;
   		 }	
         else if( gift_sensor_onb==0){  
         	j=0;
             do{
             	 delay1ms(20);
             	 i=check_6295(1);
        	 	 if(i==0){
        	 	 	//bank_6295(0); 
   					//ic_6295_ch(0x08,8,0);   //try again
   					break;
   				}
   				j++;
   			}while(j<=30);
         }
         
         if(b3==0){
         	 if( gift_sensor_onb==1  ){ //不須進洞
        			GOT_prize();
        			b3=1;
        	 }
        }
        
        i=IN8000;
    	if((i&0x20)!=0){//Y_org
    		tep_t11=1600; //8 秒
    		do{
    			if(y_pos>=300)
    				Y_go_ccw(Y_Playstep-15);
    			else
    				Y_go_ccw(Y_Playstep+10);	
    			if(tep_t11==0){
 	  				error(14);//Y軸或原點異常
 	  			}
 	  			
 	  			i=IN8000;
 	  		}while((i&0x20)!=0);//Y_org
        }
    	y_pos=0;	
    	if( gift_sensor_onb==0){
        		bank_6295(0); 
   				ic_6295_ch(0x10,8,0);   //try again
   		}
        delay100ms(2);
        
        
   		
        x_home_good();
        if(b3==0){
         	 if( gift_sensor_onb==1  ){ //不須進洞
        			GOT_prize();
        			b3=1;
        	 }
        }
        fm25writ_data8(0x001f,0x00); //	完成遊戲流程
        gift_sensor_chk=0;//停止偵測gift sensor
        ot_relay|=0x80;	//sensor off
    	gift_money_okb=0;//選務金額已滿額
    	
    delay100ms(5);
    bb=0;
    if(P35!=0)
 		sensor_check();
    mtime=0;
    gift_dsp_mode();	//啟動選物販賣顯示功能
    mub=0;	
    mk=1;
    attmusic_min=0;
 	m=attmusic_M;	
    //wait_6295(1);	
 	play_pushb=0;//推桿燈亮
  }
}